import React, { useEffect, useRef, useState } from "react";
import videojs from "video.js";
import VideoJS from "./VideoJS";

const Player = ({ upload, type = null }: { upload: any; type?: string }) => {
  const playerRef = useRef(null);
  const [src, setSrc] = useState(upload);
  const [srcType, setSrcType] = useState(type);

  useEffect(() => {
    if (type === null || type === undefined || type === "") {
      setSrcType("video/mp4");
      setSrc(`${src}#.mp4`);
    }
  }, []);

  const videoJsOptions = {
    autoplay: false,
    controls: true,
    responsive: true,
    fluid: true,
    aspectRatio: "16:9",
    preload: true,
    sources: [
      {
        type: srcType,
        src: src,
      },
    ],
    audioOnlyMode: srcType.split("/")[0] === "audio" ? true : false,
    liveui: true,
    playbackRates: [0.5, 1, 1.5, 2],
    restoreEl: true,
    controlBar: {
      skipButtons: {
        forward: 5,
        backward: 10,
      },
    },
  };

  const handlePlayerReady = (player) => {
    playerRef.current = player;

    // You can handle player events here, for example:
    player.on("waiting", () => {
      videojs.log("player is waiting");
    });

    player.on("dispose", () => {
      videojs.log("player will dispose");
    });
  };

  return (
    <div className="w-full h-full border-2 border-[#202020] border-solid rounded-lg">
      <VideoJS options={videoJsOptions} onReady={handlePlayerReady} />
    </div>
  );
};
export default Player;
