import React from "react";

export const ButtonType = {
  primary:
    "bg-blue-600 hover:bg-blue-700 active:bg-blue-800 text-white font-bold rounded shadow hover:shadow-lg",
  secondary:
    "bg-emerald-500 hover:bg-emerald-700 active:bg-emerald-800 text-white font-bold rounded shadow hover:shadow-lg",
  danger:
    "bg-red-500 hover:bg-red-700 active:bg-red-800 text-white font-bold rounded shadow hover:shadow-lg",
  tertiary: "font-bold rounded",
};

export const ButtonSize = {
  xs: "py-1 px-2 text-xs",
  sm: "py-3 px-4 text-sm",
  md: "py-4 px-6 text-md",
  lg: "py-5 px-8 text-lg",
};

const Button = ({
  id,
  size,
  type,
  children,
  onClick,
  className,
  color,
  disabled,
}: {
  id?: string;
  size: string;
  type: string;
  children: React.ReactNode;
  onClick?: (e: any) => void;
  className?: string;
  color?: string;
  disabled?: boolean;
}) => {
  return (
    <button
      id={id}
      className={`${className} text-${color}-600 active:text-${color}-700 ${ButtonType[type]} ${ButtonSize[size]} place-items-center justify-center outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 disabled:opacity-50`}
      onClick={onClick}
      disabled={disabled}
      type={type === "secondary" ? "submit" : "button"}
    >
      {children}
    </button>
  );
};

export default Button;
