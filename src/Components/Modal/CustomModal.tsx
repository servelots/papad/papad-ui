import axios from "axios";
import React, { useRef, useState } from "react";
import { useRecoilValue } from "recoil";
import { prodState, tokenState } from "../../Modules/AuthState/AuthState";
import Button from "../Button/Button";
import Toggle from "../Button/Toggle";

const CustomModal = ({
  onClick,
  onClose,
  groupId,
  groups,
  extra,
  deleteButton,
}: {
  onClick?: () => void;
  onClose: () => void;
  groupId?: string;
  groups: any;
  extra?: any;
  deleteButton?: boolean;
}) => {
  const auth = localStorage.getItem("token");
  const [toGroup, setToGroup] = useState("");
  const [addedGroup, setAddedGroup] = useState("");
  const [disGrp, setDisGrp] = useState(true);
  const [error, setError] = useState<any>([]);
  const [existingData, setExistingData] = useState(false);
  const [disEdit, setDisEdit] = useState(false);
  const [deletePrev, setDeletePrev] = useState(false);

  const fieldInput = useRef<HTMLInputElement>(null);
  const baseURL = localStorage.getItem("prod_url");

  const handleCopy = () => {
    const formData = new FormData();
    formData.append("question_id", toGroup);
    deleteButton
      ? formData.append("remove_existing_data", existingData ? "True" : "False")
      : formData.append("question", addedGroup);

    const delOptions = {
      method: "PUT",
      url: `${baseURL}api/v1/group/remove_question/${groupId}/`,
      headers: {
        "Content-Type": "multipart/form-data",
        Authorization: `Token ${auth}`,
      },
      data: formData,
    };

    const editOptions = {
      method: "PUT",
      url: `${baseURL}api/v1/group/update_question/${groupId}/`,
      headers: {
        "Content-Type": "multipart/form-data",
        Authorization: `Token ${auth}`,
      },
      data: formData,
    };

    axios
      .request(deleteButton ? delOptions : editOptions)
      .then((response) => {
        console.log(response);
        onClose();
      })
      .catch((error) => {
        console.log(error);
        setError((prev) => [...prev, error.response.data]);
      });
  };

  const Display = () => {
    return (
      <>
        <div className="p-5 flex place-items-center justify-center items-center">
          <Button
            size="sm"
            type="tertiary"
            className={
              "w-fit justify-center flex gap-5 px-5 py-3 shadow-lg rounded leading-normal text-emerald-600 bg-white hover:bg-emerald-600 hover:text-white"
            }
          >
            {addedGroup}
          </Button>
        </div>
        <div className="p-5 flex place-items-center justify-center items-center">
          <Toggle
            onClick={() => setExistingData(!existingData)}
            title="Remove existing data"
            checked={existingData}
          />
        </div>
      </>
    );
  };

  const UserGroups = () => {
    return (
      <>
        <div className="grid lg:grid-cols-4 gap-3 p-1 h-auto overflow-hidden">
          {groups.map((grp, i) => (
            <div
              className="group grid grid-cols-6 rounded-lg shadow-xl bg-white max-w-sm text-justify "
              key={i}
            >
              <div className="col-span-5 p-3 w-max-content">
                <h5 className="text-gray-900 text-xl font-medium mb-2">
                  {grp.question}
                  {grp.question_mandatory && "*"}
                </h5>
                <p className="text-gray-700 text-base mb-4 line-clamp-3 hover:line-clamp-none">
                  {grp.question_type}
                </p>
                <div className="center place-items-center justify-center">
                  {deleteButton ? (
                    <Button
                      size="sm"
                      type="danger"
                      onClick={() => {
                        setAddedGroup(grp.question);
                        setToGroup(grp.id);
                        setDisGrp(false);
                        setDeletePrev(true);
                      }}
                    >
                      Delete
                    </Button>
                  ) : (
                    <Button
                      size="sm"
                      type="secondary"
                      onClick={() => {
                        setAddedGroup(grp.question);
                        setToGroup(grp.id);
                        setDisEdit(true);
                      }}
                    >
                      Edit
                    </Button>
                  )}
                </div>
              </div>
            </div>
          ))}
        </div>
      </>
    );
  };

  const EditCustom = () => {
    return (
      <div className="md:flex md:items-center m-5 mb-6">
        <div className="md:w-1/3">
          <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-0 pr-4">
            Field
          </label>
        </div>
        <div className="md:w-2/3">
          <input
            className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
            type="text"
            ref={fieldInput}
            maxLength={300}
            value={addedGroup}
            autoFocus={true}
            onChange={(e) => {
              setAddedGroup(e.target.value);
            }}
          />
        </div>
      </div>
    );
  };

  const ErrorFeedback = () => {
    return (
      <>
        {error.length !== 0 &&
          error.map((x) => (
            <div className="flex items-center justify-center p-5 border-b border-solid border-slate-200 rounded-t">
              <h5 className="text-xl font-medium text-red-600">{x.detail}</h5>
            </div>
          ))}
      </>
    );
  };

  return (
    <div className="p-1 pt-2">
      {disGrp && <UserGroups />}
      {deletePrev && <Display />}
      {disEdit && <EditCustom />}
      {error && error !== "" && <ErrorFeedback />}
      <div className="p-5 flex flex-row place-items-center justify-center border-t-2">
        <Button size="sm" type="secondary" onClick={handleCopy}>
          Confirm
        </Button>
        <Button size="sm" type="danger" onClick={onClose}>
          Cancel
        </Button>
      </div>
    </div>
  );
};

export default CustomModal;
