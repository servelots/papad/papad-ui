import React, { useEffect, useState } from "react";
import { Space, Select, SelectProps, Divider } from "antd";
import axios from "axios";
import Button from "../Button/Button";

const AddUsersModal = ({ users, onClose, id, onClick }) => {
  const [searchedUsers, setSearchedUsers] = useState<any>([]);
  const [loading, setLoading] = useState(false);
  const [selectedItems, setSelectedItems] = useState<any[]>([]);
  const [USERS, setUSERS] = useState<any[]>([]);

  const baseURL = localStorage.getItem("prod_url");
  const auth = localStorage.getItem("token");
  const { Option } = Select;

  useEffect(() => {
    getUsersInArray();
    setSelectedItems(USERS);
  }, []);

  useEffect(() => {}, [searchedUsers]);

  const getUsersInArray = () => {
    users.map((user) => {
      USERS.push({ value: user.id, label: user.username });
    });
  };

  const filteredOptions = USERS.filter(
    (user) =>
      !selectedItems.map((selectedUser) => selectedUser.label === user.username)
  );

  const filterOption = (input, option) => {
    if (!input || !option || !option.children) {
      return false;
    }
    return option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0;
  };

  const searchBy = async (value) => {
    const options = await {
      method: "GET",
      url: `${baseURL}api/v1/users/search/`,
      params: { search: value },
      headers: {
        "Content-Type": "application/json",
        Authorization: `Token ${auth}`,
      },
    };

    axios
      .request(options)
      .then((response) => {
        setSearchedUsers(response.data.results);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const updateUser = (e) => {
    const form = new FormData();
    form.append("users", selectedItems.join(","));

    const options = {
      method: "PUT",
      url: `${baseURL}api/v1/group/${id}/`,
      headers: {
        "Content-Type": "multipart/form-data",
        Authorization: `Token ${auth}`,
      },
      data: form,
    };

    axios
      .request(options)
      .then((response) => {
        console.log(response.data);
        setLoading(false);
        onClick();
        onClose();
      })
      .catch((error) => {
        console.error(error);
        setLoading(false);
        onClose();
      });
  };

  const FormFooter = () => {
    return (
      <div className="text-center mt-40 p-5 border-t">
        <Button
          size="sm"
          className="w-full"
          type="secondary"
          disabled={loading}
          onClick={(e) => {
            updateUser(e);
            setLoading(true);
          }}
        >
          Submit
        </Button>
        <Button size="sm" type="tertiary" color="red" onClick={() => onClose()}>
          Cancel
        </Button>
      </div>
    );
  };

  return (
    <div>
      <Select
        mode="multiple"
        style={{ width: "100%" }}
        placeholder="Add users"
        value={selectedItems}
        onChange={setSelectedItems}
        onSearch={(e) => {
          console.log(e);
          e !== "" && searchBy(e);
        }}
        showSearch
        autoClearSearchValue={true}
        removeIcon={true}
        allowClear={false}
        filterOption={filterOption}
      >
        {filteredOptions?.map((user, key) => (
          <Option value={user.id} label={user.username} key={key}>
            <Space>{user.username}</Space>
          </Option>
        ))}

        {searchedUsers
          ?.filter(
            (searchUser) =>
              !users.some(
                (existingUser) => existingUser.username === searchUser.username
              )
          )
          .map((user) => (
            <Option key={user.id} value={user.id} label={user.username}>
              {user.username}
            </Option>
          ))}
      </Select>

      <FormFooter />
    </div>
  );
};

export default AddUsersModal;
