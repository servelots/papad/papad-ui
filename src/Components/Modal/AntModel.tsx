import React, { useState } from "react";
import {  Modal,  Typography } from "antd";
import TagButton from "../Button/TagButton";
import LocalTime from "../Time/LocalTime";
import AntImage from "../Image/AntImage";
import { RiTimerFlashLine } from "react-icons/ri";
import { FaUser } from "react-icons/fa";

const { Text } = Typography;

const CardProfileHeader = ({ user }) => {
  return (
    <>
      <div className="flex text-white text-sm font-bold cursor-pointer">
        <TagButton
          color="#108ee9"
          tagName={
            <div className="flex">
              <span className="self-center">
                <FaUser size={10} className="mr-2" />
              </span>
              <span> {user}</span>
            </div>
          }
        />
      </div>
    </>
  );
};
const AntModel = ({
  isButton,
  buttonSize,
  buttonType,
  buttonText,
  modelData,
  modelTitle,
  placement,
  modelOpen,
  startTime,
  endTime,
  updateModel,
  copyModel,
  okText,
  okType,
  okDisabled,
}: {
  isButton?: any;
  buttonSize?: any;
  buttonType?: any;
  buttonText?: any;
  modelData?: any;
  modelTitle: any;
  placement?: any;
  modelOpen?: boolean;
  startTime?: any;
  endTime?: any;
  updateModel?: any;
  copyModel?: any;
  okText?: any;
  okType?: any;
  okDisabled?: any;
}) => {
  const [open, setOpen] = useState(modelOpen);
  const [ellipsis, setEllipsis] = useState(true);

  return (
    <>
      {/* {isButton && 
            <Button type={buttonType} onClick={() => setOpen(true)} size={buttonSize} className={`float-${placement}`}>
                {buttonText}
            </Button>
        } */}
      <Modal
        title={
          <div className="sm:flex justify-between">
            {modelTitle && (
              <div className="hidden sm:block">
                <Text ellipsis={ellipsis ? { tooltip: { modelTitle } } : false}>
                  {modelTitle}
                </Text>
              </div>
            )}
            {startTime && (
              <div className="flex gap-2 sm:mt-0 mt-2">
                <TagButton
                  color="#108ee9"
                  tagName={
                    <div className="flex">
                      <span className="self-center">
                        <RiTimerFlashLine size={15} className="mr-2" />
                      </span>
                      <span> {startTime + " - " + endTime}</span>
                    </div>
                  }
                />
                <TagButton
                  color="#108ee9"
                  tagName={<LocalTime timeVal={modelData?.createdDate} />}
                />
              </div>
            )}
          </div>
        }
        centered
        open={open}
        okText={okText}
        okType={okType}
        okButtonProps={{ disabled: okDisabled }}
        onOk={() => {
          setOpen(false);
          updateModel("ok");
        }}
        onCancel={() => {
          setOpen(false);
          updateModel("cancel");
        }}
        closable={false}
        width={1000}
      >
        <div>
          {modelTitle && (
            <div className="sm:hidden justify-between">{modelTitle}</div>
          )}
          {modelData && (
            <div className="sm:flex justify-between bg-[#eef6ff] rounded-md pl-2">
              <div className="rounded w-fit p-1 sm:p-0 self-center text-sm mb-1">
                <div className="flex justify-between w-fit p-1 pl-0">
                  <span className="font-semibold">
                    <CardProfileHeader
                      user={
                        modelData?.createdBy?.first_name
                          ? modelData?.createdBy?.first_name +
                            " " +
                            modelData?.createdBy?.last_name
                          : "N/A"
                      }
                    />
                  </span>
                </div>
              </div>
              {modelData?.tags && (
                <div className="flex  sm:w-[20rem] rounded-md overflow-x-scroll h-12  sm:justify-end">
                  {modelData?.tags.map(
                    (tag, i) =>
                      tag.name !== "" &&
                      tag.count !== 0 && (
                        <TagButton key={i} tagName={tag.name} color="#55acee" />
                      )
                  )}
                </div>
              )}
            </div>
          )}
          {copyModel && copyModel}
          {modelData && (
            <div className="overflow-hidden hover:overflow-y-scroll text-justify pr-[10px] h-[20rem] overflow-y-scroll mt-2 border-[#d9d9d9] border-solid border rounded-md sh">
              <p className="p-2">{modelData?.text}</p>
            </div>
          )}
          {modelData?.annoImage && (
            <div className="mt-4">
              <AntImage width={200} src={modelData?.annoImage} />
            </div>
          )}

          <div></div>
        </div>
      </Modal>
    </>
  );
};

export default AntModel;
