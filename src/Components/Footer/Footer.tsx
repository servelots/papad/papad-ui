import React from "react";

const Footer = () => {
  return (
    <footer className="bg-gray-900 py-4">
      <div className="container mx-auto flex flex-col sm:flex-row justify-between px-4">
        <div className="text-gray-500 text-left order-last sm:order-first sm:w-1/2">
          <span>© 2023 </span>
          <a
            className="text-gray-600 font-normal cursor-pointer"
            href="https://open.janastu.org"
          >
            janastu.org
          </a>
          <span>, Inc. </span>
          <a
            className="text-gray-600 font-normal cursor-pointer"
            href="https://gitlab.com/servelots/papad/papad-api/-/blob/prod/LICENSE"
          >
            GNU AGPLv3
          </a>
        </div>
        <div className="text-gray-500 text-right order-first sm:order-last sm:w-1/2">
          <span className="pb-2 break-words">
            papad-version:{process.env.REACT_APP_BRANCH} | built-id:
            {process.env.REACT_APP_COMMITID}
          </span>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
