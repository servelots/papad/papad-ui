import React, { useState } from "react";
import { Typography } from "antd";

const { Paragraph, Text } = Typography;

const Paragraphs = ({
  isExpand,
  rows,
  text,
}: {
  isExpand?: boolean;
  rows?: number;
  text?: any;
}) => {
  const [ellipsis, setEllipsis] = useState(isExpand);

  return (
    <>
      <Paragraph
        ellipsis={
          ellipsis
            ? { rows: rows ? rows : 2, expandable: true, symbol: "more" }
            : false
        }
      >
        {text}
      </Paragraph>
    </>
  );
};

export default Paragraphs;
