import React, { useRef, useState } from 'react';
import { SearchOutlined } from '@ant-design/icons';
import { InputRef, Tag } from 'antd';
import { Button, Input, Space, Table } from 'antd';
import type { ColumnsType, ColumnType } from 'antd/es/table';
import type { FilterConfirmProps } from 'antd/es/table/interface';
import Highlighter from 'react-highlight-words';

const Tables = ({data}) => {
  const [searchText, setSearchText] = useState('');
  const [searchedColumn, setSearchedColumn] = useState('');
  const searchInput = useRef<InputRef>(null);

  const handleSearch = (
    selectedKeys: string[],
    confirm: (param?: FilterConfirmProps) => void,
    dataIndex: any,
  ) => {
    confirm();
    setSearchText(selectedKeys[0]);
    setSearchedColumn(dataIndex);
  };

  const handleReset = (clearFilters: () => void) => {
    clearFilters();
    setSearchText('');
  };

  const getColumnSearchProps = (dataIndex): ColumnType<any> => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters, close }) => (
      <div style={{ padding: 8 }} onKeyDown={(e) => e.stopPropagation()}>
        <Input
          ref={searchInput}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => handleSearch(selectedKeys as string[], confirm, dataIndex)}
          style={{ marginBottom: 8, display: 'block' }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => handleSearch(selectedKeys as string[], confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Search
          </Button>
          <Button
            onClick={() => clearFilters && handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </Button>
          <Button
            type="link"
            size="small"
            onClick={() => {
              confirm({ closeDropdown: false });
              setSearchText((selectedKeys as string[])[0]);
              setSearchedColumn(dataIndex);
            }}
          >
            Filter
          </Button>
          <Button
            type="link"
            size="small"
            onClick={() => {
              close();
            }}
          >
            close
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered: boolean) => (
      <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes((value as string).toLowerCase()),
    onFilterDropdownOpenChange: (visible) => {
      if (visible) {
        setTimeout(() => searchInput.current?.select(), 100);
      }
    },
    render: (text) =>
      searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
          searchWords={[searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ''}
        />
      ) : (
        text
      ),
  });

  const columns: ColumnsType<any> = [
    {
      title: 'Request Id',
      dataIndex: 'request_id',
      key: 'request_id',
      width: '25%',
      ...getColumnSearchProps('request_id'),
    },
    {
      title: 'Request Type',
      dataIndex: 'request_type',
      key: 'request_type',
      render: (_, {request_type, ie_metaData}) => (
        <Space size="middle">
          <a>{request_type}</a>
          <a>{ie_metaData}</a>
        </Space>
        ),
      ...getColumnSearchProps('request_type'),
    },
    {
      title: 'Requested At',
      dataIndex: 'requested_at',
      key: 'requested_at',
      sorter: (a, b) => a.requested_at.length - b.requested_at.length,
      sortDirections: ['descend', 'ascend'],
    },
    {
      title: 'Authorised',
      dataIndex: 'is_authorized',
      key: 'is_authorized',
      render: (_, { is_authorized }) => (
        <>
        <Tag color={is_authorized === "Yes" ? "green" : "red"} key={is_authorized}>
            {is_authorized.toString().toUpperCase()}
        </Tag>
        </>
      ),
      sorter: (a, b) => a.is_authorized.length - b.is_authorized.length,
      sortDirections: ['descend', 'ascend'],
    },
    {
      title: 'Details',
      dataIndex: 'detail',
      key: 'detail',
    //   sorter: (a, b) => a.detail.length - b.detail.length,
      sortDirections: ['descend', 'ascend'],
    },
    {
      title: 'Completed',
      dataIndex: `is_complete`,
      key: 'is_complete',
      render: (_, { is_complete }) => (
        <>
        <Tag color={is_complete === "Yes" ? "green" : "red"} key={is_complete}>
            {is_complete.toString().toUpperCase()}
        </Tag>
        </>
      ),
      sorter: (a, b) => a.is_complete.length - b.is_complete.length,
      sortDirections: ['descend', 'ascend'],
    },
    {
        title: 'Completed At',
        dataIndex: 'completed_at',
        key: 'completed_at',
        sorter: (a, b) => a.completed_at.length - b.completed_at.length,
        sortDirections: ['descend', 'ascend'],
      },
      {
        title: 'Requested File',
        dataIndex: 'request_file',
        key: 'request_file',
        // sorter: (a, b) => a.request_file.length - b.request_file.length,
        sortDirections: ['descend', 'ascend'],
      },

  ];

  return <Table scroll={{ x: 1000 }} columns={columns} dataSource={data} />;
};

export default Tables;
