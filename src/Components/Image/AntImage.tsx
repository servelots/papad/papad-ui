import React from 'react'
import { Image } from 'antd';

const AntImage = ({width, src}:{width:any,src:string}) => {
  return (
    <Image
        width={width}
        src={src}
        fallback="Faield to load image"    />
  )
}

export default AntImage