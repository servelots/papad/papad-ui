import React from "react";

const Input = ({ maxLength, placeholder }) => {
  return (
    <div>
      <div className="relative flex w-full flex-wrap items-stretch mb-3">
        <input
          type="text"
          maxLength={maxLength}
          placeholder={placeholder}
          className="px-3 py-4 placeholder-slate-300 text-slate-600 relative  bg-white rounded text-base border-0 shadow outline-none focus:outline-none focus:ring w-full pr-10"
        />
        <span className="z-10 h-full leading-snug font-normal  text-center text-slate-300 absolute bg-transparent rounded text-lg items-center justify-center w-8 right-0 pr-3 py-4">
          <i className="fas fa-user"></i>
        </span>
      </div>
    </div>
  );
};

export default Input;
