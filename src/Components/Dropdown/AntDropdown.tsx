import { Menu, Transition } from "@headlessui/react";
import React, { Fragment } from "react";
import {
  MdDeleteForever,
  MdModeEdit,
  MdOutlineFileCopy,
  MdSettings,
  MdShare,
} from "react-icons/md";
import { HiDotsVertical } from "react-icons/hi";
import { FaFileExport } from "react-icons/fa";

const AntDropDown = ({
  media,
  dropIcon = "VerticalDots",
  modelOpen,
  right = "2",
  position = "absolute",
  top = "2",
  value,
}: {
  media?: any;
  dropIcon?: any;
  right?: any;
  top?: any;
  position?: any;
  modelOpen?: any;
  value?: any;
}) => {
  const getIcon = (value: any) => {
    switch (value) {
      case "edit":
        return <MdModeEdit className="pr-2" size={25} />;
      case "delete":
        return <MdDeleteForever className="pr-2" size={25} />;
      case "copy":
        return <MdOutlineFileCopy className="pr-2" size={25} />;
      case "share":
        return <MdShare className="pr-2" size={25} />;
      case "export":
        return <FaFileExport className="pr-2" size={25} />;
    }
  };

  const menuIcon = (value: any) => {
    switch (value) {
      case "VerticalDots":
        return <HiDotsVertical size={20} />;
      case "Settings":
        return <MdSettings className="h-[1.5rem] w-[1.5rem]" size={20} />;
    }
  };

  return (
    <Menu
      as="div"
      className={`flex gap-2 ${position} top-${top} right-${right} cursor-pointer`}
    >
      <div className="items-center flex rounded-xl hover:bg-[#d6ebfd] p-2 focus:outline-none w-fit">
        <Menu.Button className="flex text-sm focus:outline-none focus:ring-none focus:ring-none">
          {menuIcon(dropIcon)}
        </Menu.Button>
      </div>
      <Transition
        as={Fragment}
        enter="transition ease-out duration-100"
        enterFrom="transform opacity-0 scale-95"
        enterTo="transform opacity-100 scale-100"
        leave="transition ease-in duration-75"
        leaveFrom="transform opacity-100 scale-100"
        leaveTo="transform opacity-0 scale-95"
      >
        <div className=" text-md font-semibold absolute right-0 px-2 z-10 mt-[2.4rem] min-w-min max-w-[100%] origin-top-right rounded-md text-black bg-white py-1 shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none border-cyan-100 border-[0.1rem]">
          <Menu.Items className="divide-y divide-[#e5e7eb] w-full">
            <Menu.Item>
              <div className="my-2">
                <div className="flex flex-row w-full my-2 text-sm font-normal">
                  <div className="flex flex-col w-full">
                    {value.map(({ id, displayName }) => (
                      <div
                        className="p-2 hover:bg-[#acd3ee] cursor-pointer flex  justify-between"
                        key={id}
                        onClick={() =>
                          modelOpen({
                            id,
                            uuid: media?.uuid,
                            name: media?.name,
                            tags: media?.tags,
                            description: media?.description,
                            created_by: media?.created_by?.first_name,
                          })
                        }
                      >
                        <span className="flex place-items-center">
                          <span>{getIcon(id)}</span>
                          <span>{displayName}</span>
                        </span>
                      </div>
                    ))}
                  </div>
                </div>
              </div>
            </Menu.Item>
          </Menu.Items>
        </div>
      </Transition>
    </Menu>
  );
};

export default AntDropDown;
