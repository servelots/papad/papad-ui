import moment from "moment";
import React, { useEffect, useState } from "react";

const LocalTime = ({ timeVal }: { timeVal: string }) => {
  const [time, setTime] = useState("");

  useEffect(() => {
    const localMoment = moment.utc(timeVal).local();
    setTime(moment(localMoment).fromNow());
  }, []);

  return <span className="self-center">{time}</span>;
};

export default LocalTime;
