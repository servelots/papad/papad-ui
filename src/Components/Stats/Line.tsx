import React, { useEffect, useState } from "react";
import ReactEcharts from "echarts-for-react";

const Line = ({ stats, label }) => {
  const [xValue, setXValue] = useState<any>([]);
  const [yValue, setYValue] = useState<any>([]);

  useEffect(() => {
    setXValue(
      stats.map((data) => {
        return data.created_date;
      })
    );
    setYValue(
      stats.map((data) => {
        return data.total;
      })
    );
  }, []);

  const option = {
    tooltip: {
      trigger: "axis",
    },
    legend: {},
    toolbox: {
      show: true,
      feature: {
        dataZoom: {
          yAxisIndex: "none",
        },
        dataView: { readOnly: true },
        magicType: { type: ["line", "bar"] },
        restore: {},
        saveAsImage: {},
      },
    },
    xAxis: {
      type: "category",
      boundaryGap: false,
      data: xValue,
    },
    yAxis: {
      type: "value",
      axisLabel: {
        formatter: yValue,
      },
    },
    series: [
      {
        name: label,
        type: "line",
        data: yValue,
        markLine: {
          data: [
            { type: "average", name: "Avg" },
            [
              {
                symbol: "none",
                x: "90%",
                yAxis: "max",
              },
              {
                symbol: "circle",
                label: {
                  position: "start",
                  formatter: "Max",
                },
                type: "max",
                name: "Maximum",
              },
            ],
            [
              {
                symbol: "none",
                x: "90%",
                yAxis: "min",
              },
              {
                symbol: "circle",
                label: {
                  position: "start",
                  formatter: "Min",
                },
                type: "min",
                name: "Minimum",
              },
            ],
          ],
        },
      },
    ],
  };

  return (
    <>
      <div className="items-center justify-center text-center place-items-center">
        <div className="col-md-12">
          <div className="font-bold text-xl">{label} Stats</div>
        </div>
      </div>
      {stats.length !== 0 && (
        <ReactEcharts
          option={option}
          style={{ height: "1000px", width: "100%" }}
        />
      )}
    </>
  );
};

export default Line;
