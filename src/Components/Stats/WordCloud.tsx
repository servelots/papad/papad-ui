import React from "react";
import ReactEcharts from "echarts-for-react";

interface GraphNode {
  symbolSize: number;
  label?: {
    show?: boolean;
  };
}

const WordCloud = ({ data, layout, title }) => {
  data.nodes.forEach((node: GraphNode) => {
    node.label = {
      show: node.symbolSize > 0,
    };
  });

  const option = {
    title: {
      text: title,
      subtext: "Word Cloud",
      top: "bottom",
      left: "right",
    },
    tooltip: {},
    legend: [
      {
        data: data?.categories.map((a: { name: string }) => {
          return a.name;
        }),
      },
    ],
    animationDurationUpdate: 1500,
    animationEasingUpdate: "quinticInOut",
    series: [
      {
        name: title,
        type: "graph",
        layout: "force",
        circular: {
          rotateLabel: true,
        },
        data: data?.nodes,
        categories: data?.categories,
        roam: true,
        label: {
          position: "right",
          formatter: "{b}",
        },
        emphasis: {
          focus: "adjacency",
        },
      },
    ],
  };

  return (
    <>
      {data.length !== 0 && (
        <ReactEcharts
          option={option}
          style={{ height: "1000px", width: "100%" }}
        />
      )}
    </>
  );
};

export default WordCloud;
