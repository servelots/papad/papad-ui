import React from "react";

const CircularLoader = () => {
  return (
    <div
      className={`fixed inset-0 w-screen h-screen z-50 flex justify-center items-center bg-black bg-opacity-50 `}
    >
      <div className="w-24 h-24 border-l-4 border-blue-600 rounded-full animate-spin"></div>
    </div>
  );
};

export default CircularLoader;
