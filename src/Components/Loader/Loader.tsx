import React from "react";
import Button from "../Button/Button";

const Loader = ({ error }: { error?: string }) => {
  return (
    <div className="h-screen bg-transparent p-10 grid grid-cols-1 ">
      <div className="place-self-center">
        <div className="grid items-center justify-center place-items-center gap-5">
          <div className="w-24 h-24 border-l-4 border-blue-600 rounded-full animate-spin"></div>
          <div className="p-5">{error}</div>
          {error !== "" ? null : (
            <div className="p-5 flex">
              <Button
                size="sm"
                type="secondary"
                onClick={() => window.location.reload}
              >
                Retry
              </Button>
              <Button
                size="sm"
                type="tertiary"
                color="red"
                onClick={() => window.location.reload}
              >
                View Details
              </Button>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default Loader;
