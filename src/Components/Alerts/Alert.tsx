import React, { useState } from "react";
import Button from "../Button/Button";

export const AlertType = {
  primary:
    "bg-blue-600 hover:bg-blue-700 active:bg-blue-800 text-white font-bold rounded uppercase shadow hover:shadow-lg",
  success:
    "bg-emerald-500 hover:bg-emerald-700 active:bg-emerald-800 text-white font-bold rounded uppercase shadow hover:shadow-lg",
  danger:
    "bg-red-500 hover:bg-red-700 active:bg-red-800 text-white font-bold rounded uppercase shadow hover:shadow-lg",
  tertiary: "font-bold rounded",
};

const Alert = ({
  message,
  className,
  color,
  type,
}: {
  message: string;
  className?: string;
  color?: string;
  type: string;
}) => {
  const [showAlert, setShowAlert] = useState(true);
  return (
    <>
      {showAlert ? (
        <div
          className={`${className} text-${color}-600 active:text-${color}-700 ${AlertType[type]} place-items-center justify-center outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 disabled:opacity-50`}
        >
          <span className="text-xl inline-block mr-5 align-middle">
            <i className="fas fa-bell" />
          </span>
          <span className="inline-block align-middle mr-8">{message}</span>
          <Button
            className="absolute bg-transparent text-2xl font-semibold leading-none right-0 top-0 mt-4 mr-6 outline-none focus:outline-none"
            onClick={() => setShowAlert(false)}
            size="sm"
            type="secondary"
          >
            <span>×</span>
          </Button>
        </div>
      ) : null}
    </>
  );
};

export default Alert;
