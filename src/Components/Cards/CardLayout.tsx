import React, { useState, useEffect } from "react";
import { Card, Col, Row, Space, Typography, Button } from "antd";
import { useNavigate, useParams } from "react-router-dom";
import IconButton from "../Button/IconButton";
import { MdLock, MdPermIdentity } from "react-icons/md";
import TagButton from "../Button/TagButton";
import Paragraphs from "../Paragraph/Paragraphs";
import { FaUser } from "react-icons/fa";
import { MdOutlineUpdate } from "react-icons/md";
import TimeAgo from "react-timeago";
import LocalTime from "../Time/LocalTime";

const { Paragraph, Text } = Typography;

const CardLayout = ({
  cardData,
  collection,
  media,
  dropdown,
  id,
  noDataText,
  groups,
  onSelect,
  loading,
}: {
  cardData?: any;
  collection?: any;
  media?: any;
  dropdown?: any;
  id?: any;
  noDataText?: any;
  groups?: any;
  onSelect?: any;
  loading?: boolean;
}) => {
  const [iconColor, setIconColor] = useState("");
  const navigate = useNavigate();

  useEffect(() => {
    collection && collection?.is_public
      ? setIconColor("bg-emerald-500")
      : setIconColor("bg-blue-500");
  }, []);

  const CardProfileHeader = ({ user }) => {
    return (
      <>
        <div className="flex text-sm">
          <div className="flex">
            <span className="self-center">
              <FaUser size={10} className="mr-2" />
            </span>
            <span>{user}</span>
          </div>
        </div>
      </>
    );
  };

  return (
    <>
      {media && (
        <Card style={{ borderColor: "rgb(179, 222, 236)" }} loading={loading}>
          <div className="flex justify-between gap-2 mb-2 mr-2 items-center">
            <div
              className="font-semibold cursor-pointer"
              onClick={() => navigate(`/collection/${id}/media/${media?.uuid}`)}
            >
              <Text ellipsis={false} className="cursor-pointer">
                {media?.name}
              </Text>
            </div>
            <div>{dropdown}</div>
          </div>
          <hr />
          <div className="max-h-[7rem] overflow-hidden overflow-y-auto scrollbar-hide hover:scrollbar-auto text-justify pr-[10px]">
            <Paragraphs isExpand={false} rows={4} text={media?.description} />
          </div>
          <div className="flex w-full">
            <div className="flex h-12 overflow-x-auto scrollbar-hide hover:scrollbar-auto">
              {media?.tags.map(
                (tag, i) =>
                  tag.name !== "" &&
                  tag.count !== 0 && (
                    <TagButton tagName={tag?.name} color="#55acee" key={i} />
                  )
              )}
            </div>
          </div>
          <div className="flex justify-between mb-2 place-content-center items-center">
            <div
              className={`flex space-x-2 rounded-md mx-[-0.8rem] mb-[-0.8rem] p-[0.5rem]`}
            >
              <CardProfileHeader user={media?.created_by?.username} />
              <span>{"|"}</span>
              <span className="self-center">
                <MdOutlineUpdate size={15} />
              </span>
              <LocalTime timeVal={media?.created_at} />
            </div>
            <div className="justify-end float-right mt-4 text-white text-center inline-flex w-8 h-8 shadow-lg rounded-full">
              <Button
                type="text"
                className="border-blue-500 border-2 rounded-full cursor-pointer"
                onClick={() =>
                  navigate(`/collection/${id}/media/${media?.uuid}`)
                }
              >
                View
              </Button>
            </div>
          </div>
        </Card>
      )}
      {groups && (
        <Card style={{ borderColor: "rgb(179, 222, 236)" }} loading={loading}>
          <div className="flex justify-between mb-2 mr-2 items-center">
            <div className="font-semibold">
              <Text ellipsis={false}>{groups?.name}</Text>
            </div>
          </div>
          <hr />
          <div className="h-[10rem] overflow-y-scroll  text-justify pr-[10px] mt-2 font-normal">
            <Paragraph ellipsis={false}>{groups?.description}</Paragraph>
          </div>
          <div></div>
          <div className="justify-end float-right">
            <Button
              type="text"
              className="border-blue-500 border-2 rounded-full"
              onClick={() => onSelect(groups)}
            >
              Select
            </Button>
          </div>
        </Card>
      )}
      {collection && (
        <Card style={{ borderColor: "rgb(179, 222, 236)" }} loading={loading}>
          <div className="flex justify-between mb-2 place-content-center items-center">
            <div
              className="font-semibold cursor-pointer"
              onClick={() => navigate(`/collection/${collection?.id}`)}
            >
              <Text ellipsis={false} className="cursor-pointer">
                {collection?.name}
              </Text>
            </div>
            <div
              className={
                "text-white text-center inline-flex items-center justify-center w-8 h-8 shadow-lg rounded-full " +
                iconColor
              }
            >
              {collection?.is_public ? (
                <IconButton
                  size="xs"
                  // className="p-1 text-white rounded shadow hover:shadow-lg outline-none focus:outline-none w-auto"
                  type="tertiary"
                  onClick={() => {}}
                >
                  <MdPermIdentity size={25} />
                </IconButton>
              ) : (
                <IconButton
                  size="xs"
                  // className="p-1 text-white rounded shadow hover:shadow-lg outline-none focus:outline-none w-auto"
                  type="tertiary"
                  onClick={() => {}}
                >
                  <MdLock size={15} />
                </IconButton>
              )}
            </div>
          </div>
          <hr />
          <div className="h-[7rem] overflow-y-auto  text-justify pr-[10px] mt-2 font-normal">
            <Paragraph ellipsis={false}>{collection?.description}</Paragraph>
          </div>
          <div className="flex justify-between mb-2 place-content-center items-center">
            <div
              className={`flex space-x-2 rounded-md mx-[-0.8rem] mb-[-0.8rem] p-[0.5rem]`}
            >
              <span className="self-center">
                <MdOutlineUpdate size={15} />
              </span>
              <LocalTime timeVal={collection?.created_at} />
            </div>

            <div className="justify-end float-right mt-4 text-white text-center inline-flex w-8 h-8 shadow-lg rounded-full">
              <Button
                type="text"
                className="border-blue-500 border-2 rounded-full"
                onClick={() => navigate(`/collection/${collection?.id}`)}
              >
                Open
              </Button>
            </div>
          </div>
        </Card>
      )}
      {noDataText && (
        <Card style={{ borderColor: "rgb(179, 222, 236)" }} loading={loading}>
          {noDataText}
        </Card>
      )}
      {cardData && (
        <Card
          style={{ borderColor: "rgb(179, 222, 236)" }}
          loading={loading}
        ></Card>
      )}
    </>
  );
};

export default CardLayout;
