import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { MdOutlineUpdate } from "react-icons/md";
import Dropdown from "../Dropdown/Dropdown";
import { RiTimerFlashLine } from "react-icons/ri";
import { useRecoilState } from "recoil";
import {
  cardEndTimeState,
  cardStartTimeState,
  editAnnoIDState,
} from "../../Modules/AuthState/AuthState";
import { Badge, Card, Space } from "antd";
import parse from "html-react-parser";
import TagButton from "../Button/TagButton";
import AntModel from "../Modal/AntModel";
import { CgArrowsExpandRight } from "react-icons/cg";
import { FaUser } from "react-icons/fa";
import { Image } from "antd";
import AntDropDown from "../Dropdown/AntDropdown";
import { useNavigate, useParams } from "react-router-dom";
import Paragraphs from "../Paragraph/Paragraphs";
import LocalTime from "../Time/LocalTime";

const AnnotationCard = ({
  annoText,
  createdDate,
  annotationId,
  tags,
  startTime,
  endTime,
  createdBy,
  updatedDate,
  annoImage,
  shareModal,
  deleteModal,
  exportAnno,
  mediaTitle,
  collection,
  loading,
}: {
  annoText?: string;
  createdDate?: string;
  annotationId?: string;
  tags?: any;
  startTime?: string;
  endTime?: string;
  createdBy?: any;
  updatedDate?: string;
  annoImage?: string;
  mediaTitle?: string;
  shareModal?;
  deleteModal?;
  exportAnno?;
  collection?;
  loading?: boolean;
}) => {
  const auth = localStorage.getItem("token");
  const url = window.location.pathname;
  const [cardStartTime, setCardStartTime] =
    useRecoilState<string>(cardStartTimeState);
  const [cardEndTime, setCardEndTime] =
    useRecoilState<string>(cardEndTimeState);
  const [editAnno, setEditAnno] = useRecoilState<string>(editAnnoIDState);
  const userUUID = localStorage.getItem("userUUID");
  const navigate = useNavigate();

  const { annoId } = useParams();
  const handleDeleteAnno = () => deleteModal(annotationId);
  const handleExportAnno = () => exportAnno(annotationId);
  const [modelOpen, setModelOpen] = useState(false);

  let Url = url.split("/annotation");

  useEffect(() => {}, [modelOpen]);

  const closeModel = () => {
    setModelOpen(false);
  };

  const CardHeader = ({ startTime, endTime }) => {
    return (
      <>
        <div
          className="flex text-white text-sm font-bold cursor-pointer"
          onClick={() => {
            navigate(`${Url[0]}/annotation/${annotationId}`);
            setCardStartTime(startTime);
            setCardEndTime(endTime);
          }}
        >
          <span className="self-center">
            <RiTimerFlashLine size={15} className="mr-2" />
          </span>
          <span> {startTime + " - " + endTime}</span>
        </div>
      </>
    );
  };

  const CardProfileHeader = ({ user }) => {
    return (
      <>
        <div className="flex text-sm">
          <div className="flex">
            <span className="self-center">
              <FaUser size={10} className="mr-2" />
            </span>
            <span> {user}</span>
          </div>
        </div>
      </>
    );
  };

  const CardDropDown = () => {
    return (
      <>
        <div className="-ml-16 items-end justify-end">
          {/* <Dropdown>
            <div className="pt-2 px-1 py-0 flex flex-col gap-2 overflow-auto">
              <Button
                className="w-fit h-full rounded-full px-1 flex gap-2"
                type="tertiary"
                size="xs"
                color="emerald"
                onClick={() => {
                  setEditAnno(annotationId);
                }}
              >
                <MdEdit size={15} /> Edit
              </Button>
              <Button
                className="w-fit h-full rounded-full px-1 flex gap-2"
                type="tertiary"
                size="xs"
                color="emerald"
                onClick={shareModal}
              >
                <MdOutlineShare size={15} /> Share
              </Button>
              <Button
                className="w-fit h-full rounded-full px-1 flex gap-2"
                type="tertiary"
                size="xs"
                color="red"
                onClick={handleDeleteAnno}
              >
                <MdOutlineDelete size={15} /> Delete
              </Button>
              <Button
                size="xs"
                type="tertiary"
                className="w-fit h-full rounded-full px-1 flex gap-2"
                onClick={handleExportAnno}
                color="emerald"
              >
                <BiExport size={15} /> Export
              </Button>
            </div>
          </Dropdown> */}
        </div>
      </>
    );
  };

  const openModel = (value) => {
    if (value?.id === "edit") {
      setEditAnno(annotationId);
      navigate(`${Url[0]}/annotation/${annotationId}`);
    } else if (value?.id === "share") {
      shareModal(annotationId);
    } else if (value?.id === "delete") {
      handleDeleteAnno();
    } else if (value?.id === "export") {
      handleExportAnno();
    }
  };

  return (
    <>
      <Space
        direction="vertical"
        size="middle"
        style={{ width: "100%" }}
        className={`${
          annoId === annotationId
            ? "border-green-400 shadow-xl border-[2px]"
            : "border-[#b3deec] border-[2px] shadow-sm"
        } rounded-lg`}
      >
        <Badge.Ribbon
          placement="start"
          text={<CardHeader startTime={startTime} endTime={endTime} />}
        >
          <Card
            size="small"
            loading={loading}
            extra={
              auth &&
              collection?.users?.map((user, i) => {
                return (
                  user.id.includes(userUUID) && (
                    <div className="flex gap-2" key={i}>
                      <AntDropDown
                        position="relative"
                        top="0"
                        right="2rem"
                        dropIcon={"VerticalDots"}
                        modelOpen={openModel}
                        value={[
                          { id: "edit", displayName: "Edit" },
                          { id: "delete", displayName: "Delete" },
                          { id: "share", displayName: "Share" },
                          { id: "export", displayName: "Export" },
                        ]}
                      />
                      <div
                        className="self-center cursor-pointer"
                        onClick={() => setModelOpen(true)}
                      >
                        <CgArrowsExpandRight />
                      </div>
                    </div>
                  )
                );
              })
            }
          >
            <div className={`${!auth && "mt-[2rem]"}`}>
              <div className="max-h-[7rem] overflow-hidden overflow-y-auto scrollbar-hide hover:scrollbar-auto text-justify pr-[10px]">
                <Paragraphs isExpand={false} rows={4} text={parse(annoText)} />
              </div>
              <div className="mt-6">
                <Image width={100} src={annoImage} />
              </div>
              <div className="flex w-full">
                <div className="flex h-12 overflow-x-auto scrollbar-hide hover:scrollbar-auto">
                  {tags.map(
                    (tag, i) =>
                      tag.name !== "" && (
                        <TagButton tagName={tag.name} color="#55acee" key={i} />
                      )
                  )}
                </div>
              </div>
              {/* <hr className="border border-t-[1px] border-solid border-slate-200 my-2"/> */}
              <div
                className={`flex space-x-2 mt-2 rounded-md mx-[-0.8rem] mb-[-0.8rem] p-[0.3rem]`}
              >
                <CardProfileHeader user={createdBy?.username} />
                <span>{"|"}</span>
                <span className="self-center">
                  <MdOutlineUpdate size={15} />
                </span>
                <LocalTime timeVal={createdDate} />
              </div>
            </div>
            {modelOpen && (
              <AntModel
                modelTitle={mediaTitle}
                isButton={true}
                modelOpen={modelOpen}
                updateModel={closeModel}
                buttonSize="small"
                buttonType="primary"
                placement={"right"}
                buttonText="View"
                startTime={startTime}
                endTime={endTime}
                modelData={{
                  text: parse(annoText),
                  tags: tags,
                  createdDate: createdDate,
                  createdBy: createdBy,
                  annoImage: annoImage,
                }}
              />
            )}
          </Card>
        </Badge.Ribbon>
      </Space>
    </>
  );
};

export default AnnotationCard;

AnnotationCard.defaultProps = {
  annoText: "Annotation",
  createdDate: "",
  annotionId: "",
  tags: [],
  startTime: "",
  endTime: "",
  createdBy: "",
  updatedDate: "",
  annoImage: "",
};

AnnotationCard.propTypes = {
  annoText: PropTypes.string,
  createdDate: PropTypes.string,
  annotionId: PropTypes.any,
  tags: PropTypes.array,
  startTime: PropTypes.string,
  endTime: PropTypes.string,
  updatedDate: PropTypes.string,
  annoImage: PropTypes.any,
};
