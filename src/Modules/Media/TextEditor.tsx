import React, { useState, useEffect } from "react";
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';

const TextEditor = ({
  updateTextFeild,
  editValue,
}: {
  updateTextFeild;
  editValue: string;
}) => {
  const [value, setValue] = useState(editValue);

 
  const onChange = (value) => {
    setValue(value);
    updateTextFeild(value.toString("html"));
  };
  useEffect(()=>{
    setValue(editValue.toString())
  },[editValue])

  return (
    <ReactQuill
      theme="snow"
      modules={
        {toolbar: [
          [{ 'header': '1'}, {'header': '2'}, { 'font': [] }],
          [{size: []}],
          ['bold', 'italic', 'underline', 'strike', 'blockquote'],
          [{'list': 'ordered'}, {'list': 'bullet'}, 
           {'indent': '-1'}, {'indent': '+1'}],
          ['link', 'image', 'video'],
          ['clean']
        ],
        clipboard: {
          // toggle to add extra line breaks when pasting HTML:
          matchVisual: true,
        }}
      }
      formats={[
        'header', 'font', 'size',
        'bold', 'italic', 'underline', 'strike', 'blockquote',
        'list', 'bullet', 'indent',
        'link', 'image', 'video'
      ]}
      bounds={'.app'}
      placeholder={"please provide your annotation content here....."}
      value={value}
      onChange={onChange} 
    />
  );
};

export default TextEditor;
