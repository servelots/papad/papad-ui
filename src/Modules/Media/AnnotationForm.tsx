import axios from "axios";
import React, { useCallback, useEffect, useState } from "react";
import { MdClose } from "react-icons/md";
import { VscFileMedia } from "react-icons/vsc";
import { useParams } from "react-router-dom";
import { Button, IconButton } from "../../Components";
import Dropzone from "react-dropzone";

const AnnotationForm = ({
  setUpdateAnnotate,
  onClose,
}: {
  setUpdateAnnotate?: any;
  onClose: () => void;
}) => {
  const { id } = useParams();
  const [uuid, setUuid] = useState("");
  const [text, setText] = useState("");
  const [media, setMedia] = useState("");
  const [upload, setUpload] = useState<any>();
  const [uploaded, setUploaded] = useState(false);
  const [tags, setTags] = useState("");
  const [loading, setLoading] = useState(false);
  const [mediaTarget, setMediaTarget] = useState<any>("");
  const maxWords = 250;
  const [{ content, wordCount }, setContent] = React.useState({
    content: text,
    wordCount: 0,
  });

  const auth = localStorage.getItem("token");
  const start = localStorage.getItem("startTime");
  const end = localStorage.getItem("endTime");
  const baseURL = localStorage.getItem("prod_url");

  useEffect(() => {
    if (id !== undefined) {
      setUuid(id);
      setMedia(mediaTarget);
    }
    if (start !== null || end !== null) {
      setMediaTarget("t=" + start + "," + end);
    }
  }, []);

  const handleUpload = (file) => {
    setUpload(file[0]);
  };

  const postRecord = (e) => {
    e.preventDefault();

    const formData = new FormData();
    formData.append("media_reference_id", uuid);
    formData.append("annotation_image", upload);
    formData.append("annotation_text", text);
    formData.append("media_target", "t=" + start + "," + end);
    formData.append("tags", tags);

    const options = {
      method: "POST",
      url: `${baseURL}api/v1/annotate/`,
      headers: {
        "Content-Type": "multipart/form-data",
        Authorization: `Token ${auth}`,
      },
      data: formData,
    };

    axios
      .request(options)
      .then((data) => {
        console.log(data);
        setUpdateAnnotate(data);
        localStorage.removeItem("startTime");
        localStorage.removeItem("endTime");
        onClose();
      })
      .catch((error) => {
        console.error(error);
        setLoading(false);
      });
  };

  const secToTimestamp = (sec) => {
    const date = new Date(sec * 1000);
    var hh = date.getUTCHours();
    var mm = date.getUTCMinutes();
    var ss = date.getUTCSeconds();

    if (hh < 10) {
      hh = 0 + hh;
    }
    if (mm < 10) {
      mm = 0 + mm;
    }
    if (ss < 10) {
      ss = 0 + ss;
    }
    var time = hh + ":" + mm + ":" + ss;

    return time;
  };

  const setFormattedContent = useCallback(
    (text) => {
      let words = text.split(" ").filter(Boolean);
      if (words.length > maxWords) {
        setContent({
          content: words.slice(0, maxWords).join(" "),
          wordCount: maxWords,
        });
      } else {
        setContent({ content: text, wordCount: words.length });
      }
    },
    [maxWords, setContent]
  );

  const Upload = () => {
    return (
      <div className="flex items-center justify-center place-items-center w-full mb-5">
        {!uploaded ? (
          <Dropzone
            onDrop={(e) => {
              handleUpload(e);
              setUploaded(true);
            }}
          >
            {({ getRootProps, getInputProps, isDragActive }) => (
              <div
                className="flex flex-col w-full h-full mx-auto border-4 border-dashed hover:bg-blue-100 hover:border-blue-300 text-center items-center place-items-center justify-center"
                {...getRootProps()}
              >
                <div className="flex mx-5 my-10">
                  <VscFileMedia size={30} />
                  <input
                    type="image"
                    alt="uploaded image"
                    placeholder="Select a image"
                    className="form-control block w-full px-5 py-5 text-xl font-normal text-gray-700 m-0 focus:text-gray-700 fous:bg-white focus:bg-none focus:b-none focus:outline-none"
                    onChange={(e) => {
                      handleUpload(e);
                      setUploaded(true);
                    }}
                    {...getInputProps()}
                    required
                  />
                  {isDragActive
                    ? "Drop it here to upload your media"
                    : "Click me or drag a file to upload your media"}
                </div>
              </div>
            )}
          </Dropzone>
        ) : (
          upload && (
            <div className="flex w-full">
              <div className="flex flex-col w-full h-full mx-auto border-4 item-center justify-center place-items-center border-dashed hover:bg-blue-100 hover:border-blue-300 p-2">
                <img
                  className="lg:w-80 sm:w-40 h-full max-h-max rounded-t-md bg-center"
                  loading="lazy"
                  src={URL.createObjectURL(upload)}
                  alt={upload.name}
                />
                {upload.name}
              </div>
              <div className="-ml-10 -mb-2">
                <IconButton
                  type="tertiary"
                  size="xs"
                  onClick={() => {
                    setUpload("");
                    setUploaded(false);
                  }}
                  color="black"
                >
                  <MdClose size={25} />
                </IconButton>
              </div>
            </div>
          )
        )}
      </div>
    );
  };

  return (
    <div className="justify-center">
      <div className="w-full">
        <div className="m-4">
          <form>
            <div className="md:flex md:items-center m-5 mb-6">
              <div className="md:w-1/3">
                <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-0 pr-4">
                  Text
                </label>
              </div>

              <div className="md:w-2/3 relative">
                <textarea
                  className="bg-gray-200 border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 focus:outline-none focus:bg-white focus:border-purple-500"
                  rows={5}
                  value={content}
                  onChange={(e) => {
                    setFormattedContent(e.target.value);
                    setText(e.target.value);
                  }}
                  required
                />
                <span className="font-normal text-xs text-blueGray-400 absolute bottom-0 right-0 p-2">
                  {wordCount}/{maxWords}
                </span>
              </div>
            </div>
            <div className="md:flex md:items-center m-5 mb-6">
              <div className="md:w-1/3">
                <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-0 pr-4">
                  Time Stamp
                </label>
              </div>
              <div className="md:w-2/3">
                <input
                  className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                  type="text"
                  value={`${secToTimestamp(start)} - ${secToTimestamp(end)}`}
                  disabled={true}
                />
              </div>
            </div>
            <div className="md:flex md:items-center m-5 mb-6">
              <div className="md:w-1/3">
                <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-0 pr-4">
                  Tags
                </label>
              </div>
              <div className="md:w-2/3">
                <input
                  className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                  type="text"
                  value={tags}
                  onChange={(e) => {
                    setTags(e.target.value);
                  }}
                />
              </div>
            </div>

            <Upload />
            <div className="flex items-center justify-center gap-5 p-6 border-t border-solid border-slate-200 rounded-b">
              <Button
                className="px-4 py-2 text-white bg-green-500 rounded shadow-xl uppercase"
                onClick={(e) => {
                  postRecord(e);
                  setLoading(true);
                }}
                size="sm"
                type="secondary"
                disabled={loading}
              >
                Annotate
              </Button>
              <Button size="sm" type="danger" onClick={onClose}>
                Cancel
              </Button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default AnnotationForm;
