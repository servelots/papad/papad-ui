import {
  Button,
  Collapse,
  Space,
  Tooltip,
  Select,
  notification,
  Typography,
  Descriptions,
} from "antd";
import React, { useState, useEffect } from "react";
import Annotation from "./Annotation";
import Media from "./Media";
import { SearchOutlined } from "@ant-design/icons";
import { useNavigate, useParams } from "react-router-dom";
import axios from "axios";
import TagButton from "../../Components/Button/TagButton";
import { CopyModal, DialogModal, Loader, Toggle } from "../../Components";
import { MdHome, MdOutlineUpdate } from "react-icons/md";
import { FaUser } from "react-icons/fa";
import EditUploadForm from "../Collection/EditUploadForm";
import AntDropDown from "../../Components/Dropdown/AntDropdown";
import Paragraphs from "../../Components/Paragraph/Paragraphs";
import LocalTime from "../../Components/Time/LocalTime";
import AntModel from "../../Components/Modal/AntModel";
import { useRecoilValue } from "recoil";
import { authData } from "../AuthState/AuthState";
const { Text } = Typography;

type NotificationType = "success" | "info" | "warning" | "error";

interface AuthData {
  first_name: string;
  groups: string[];
  id: string;
  is_superuser: boolean;
  last_name: string;
  username: string;
}

const AnnotationBlock = () => {
  const { Panel } = Collapse;
  const navigate = useNavigate();
  const { id, annoId } = useParams();
  const [annotations, setAnnotations] = useState<any>([]);
  const [media, setMedia] = useState<any>([]);
  const [upload, setUpload] = useState<any>([]);
  const [loading, setLoading] = useState(true);
  const [search, setSearch] = useState(false);
  const auth = localStorage.getItem("token");
  const baseURL = localStorage.getItem("prod_url");
  const [grp, setGrp] = useState<any>({});
  const [sortAnno, setSortAnno] = useState<string>("last");
  const annoSort = localStorage.getItem("annoSort");
  const userUUID = localStorage.getItem("userUUID");
  const [viewMyAnno, setViewMyAnno] = useState(false);
  const [collectionUsers, setCollectionUsers] = useState<any>([]);
  const [selectedItems, setSelectedItems] = useState<any[]>([]);
  const [USERS, setUSERS] = useState<any[]>([]);
  const [error, setError] = useState<string>("");
  const [playerType, setPlayerType] = useState<any>("Media");
  const [editModal, setEditModal] = useState(false);
  const [api, contextHolder] = notification.useNotification();
  const [modelOpen, setModelOpen] = useState(false);
  const [copyModal, setCopyModal] = useState(false);
  const [copyExtra, setCopyExtra] = useState<any[]>([]);
  const [deleteMediaModal, setDeleteMediaModal] = useState(false);
  const authDetails = useRecoilValue<AuthData>(authData);

  useEffect(() => {
    getUpload();
    getAnnotations();
    if (sortAnno !== "") {
      getAnnotations();
    }
    getUsersInArray();
  }, []);

  useEffect(() => {
    collectionUsers.length !== 0 && getUsersInArray();
  }, [collectionUsers]);

  const getUsersInArray = () => {
    collectionUsers.map((user) => USERS.push(user.username));
  };

  useEffect(() => {
    localStorage.setItem("annoSort", sortAnno);
    getAnnotations();
  }, [sortAnno, viewMyAnno, selectedItems]);

  useEffect(() => {
    annoId && setSortAnno("default");
  }, [annoId]);

  const openNotificationWithIcon = (type: NotificationType, msg) => {
    api[type]({
      message: msg,
      placement: "topRight",
    });
  };

  const notifyToast = (id) => {
    switch (id) {
      case "success":
        openNotificationWithIcon("success", "Successfully Uploaded Media");
        break;
      case "error":
        openNotificationWithIcon("error", "Failed to Uploaded Media");
        break;
    }
  };

  const getUpload = () => {
    const options = {
      method: "GET",
      url: `${baseURL}api/v1/archive/${id}/`,
    };

    axios
      .request(options)
      .then((response) => {
        if (!auth && !response.data?.group?.is_public) {
          window.location.href = "/auth/login";
        }
        setUpload(response.data.upload);
        setGrp(response.data.group);
        setCollectionUsers(response.data.group.users);
        setMedia(response.data);
      })
      .catch((e) => {
        console.log(e);
        setError(e?.message);
        setLoading(false);
      });
  };

  const getAnnotations = () => {
    const options = {
      method: "GET",
      url: `${baseURL}api/v1/archive/${id}/`,
    };

    axios
      .request(options)
      .then((response) => {
        switch (sortAnno) {
          case "last":
            setAnnotations(response.data.annotations.reverse());
            break;

          case "time":
            setAnnotations(
              response.data.annotations.sort((val1, val2) => {
                if (
                  parseInt(val1.media_target.split("=")[1].split(",")[0]) <
                  parseInt(val2.media_target.split("=")[1].split(",")[0])
                ) {
                  return -1;
                } else if (
                  parseInt(val1.media_target.split("=")[1].split(",")[0]) >
                  parseInt(val2.media_target.split("=")[1].split(",")[0])
                ) {
                  return 1;
                }
                return 0;
              })
            );
            break;
          case "default":
            let restAnno = [];
            let finalAnno = [];
            response.data.annotations.filter((anno) => {
              if (anno?.uuid === annoId) {
                finalAnno.push(anno);
              } else {
                restAnno.push(anno);
              }
            });
            restAnno.map((anno) => finalAnno.push(anno));
            setAnnotations(finalAnno);
            break;
          default:
            setAnnotations(response.data.annotations.reverse());
            break;
        }

        viewMyAnno &&
          setAnnotations(
            response.data?.annotations.filter(
              (x) => x.created_by && x.created_by.id === userUUID
            )
          );

        selectedItems.length !== 0 &&
          console.log(
            response.data?.annotations.filter(
              (x) =>
                x.created_by && x.created_by.username === USERS.map((y) => y)
            )
          );

        setLoading(false);
      })
      .catch((e) => {
        console.log(e);
        setError(e?.message);
      });
  };

  const handleMedia = (mediaId) => {
    const options = {
      method: "DELETE",
      url: `${baseURL}api/v1/archive/${mediaId}/`,
      headers: {
        Authorization: `Token ${auth}`,
      },
    };

    axios
      .request(options)
      .then((response) => {
        console.log(response);
        setDeleteMediaModal(false);
        setLoading(false);
        window.location.href = `/collection/${grp?.id}`;
      })
      .catch((e) => {
        console.log(e);
        setError(e.response.data.detail);
        setLoading(false);
      });
  };

  const updateAnnotate = () => {
    getAnnotations();
  };

  const updateGroup = () => {
    getUpload();
  };

  const handleEditMedia = () => {
    setEditModal(true);
  };

  const CardProfileHeader = ({ user }) => {
    return (
      <>
        <div className="flex text-sm cursor-pointer">
          <div className="flex">
            <span className="self-center">
              <FaUser size={10} className="mr-2" />
            </span>
            <span> {user}</span>
          </div>
        </div>
      </>
    );
  };

  const handleExport = (id: any, exporting: any) => {
    const options = {
      method: "POST",
      url: `${baseURL}api/v1/export/`,
      headers: {
        Authorization: `Token ${auth}`,
        "Content-Type": "application/json",
      },
      data: { type: exporting, id: id },
    };
    axios
      .request(options)
      .then((response) => {
        console.log(response.data);
        navigate("/requests");
      })
      .catch((error) => {
        console.error(error);
        setError(error.response.data);
      });
  };

  const openModel = (value) => {
    if (value?.id === "edit") {
      handleEditMedia();
    } else if (value?.id === "export") {
      handleExport(userUUID, "annotation");
    } else if (value?.id === "copy") {
      setCopyModal(true);
      setModelOpen(true);
    } else if (value?.id === "delete") {
      setDeleteMediaModal(true);
      setModelOpen(true);
    }
  };

  const handleChildElementClick = (e) => {
    e.stopPropagation();
  };

  const updateModel = (value) => {
    if (value === "ok") {
      if (deleteMediaModal) {
        handleMedia(media?.uuid);
      }
      if (copyModal) {
      }
    }
    setModelOpen(false);
    setDeleteMediaModal(false);
    setCopyModal(false);
    setEditModal(false);
  };

  if (error) {
    return <Loader error={error} />;
  }

  if (loading) {
    return <Loader />;
  }

  return (
    <div className="container relative p-5">
      <div className="flex space-x-1 pb-2">
        <div className="self-center cursor-pointer">
          <a
            href={`/`}
            className="text-gray-500 text-md flex gap-1 px-1 hover:bg-gray-300 hover:text-black rounded"
          >
            <MdHome
              size={16}
              className="mt-[0.2rem]"
              style={{ color: "#1677ff" }}
            />
            <Tooltip title={"Back to Home"}>
              <Text style={{ width: 38, color: "#1677ff" }}>{"Home"}</Text>
            </Tooltip>
          </a>
        </div>
        <div className="text-gray-500">{"/"}</div>
        <div className="self-center cursor-pointer">
          <Tooltip title={`Back to ${grp?.name}`}>
            <a
              href={`/collection/${grp?.id}`}
              className="text-[#1677ff] text-md flex gap-1 px-1 hover:bg-gray-300 hover:text-black rounded"
            >
              <Text
                className="sm:w-[100%] w-full max-w-5"
                ellipsis={{ tooltip: `${grp?.name}` }}
                style={{ width: "100%", color: "#1677ff", maxWidth: 100 }}
              >
                {grp?.name}
              </Text>
            </a>
          </Tooltip>
        </div>
        <div className="text-gray-500">{"/"}</div>
        <div className="text-gray-500 rounded">
          <Tooltip title={media?.name}>
            <Text
              className="sm:w-[100%] w-[30%]"
              ellipsis={{ tooltip: `${media?.name}` }}
              style={{ width: "100%", maxWidth: 150 }}
            >
              {media?.name}
            </Text>
          </Tooltip>
        </div>
      </div>

      <hr className="border-gray-300" />
      <div className="grid grid-cols-1 sm:gap-2 sm:mb-2 sm:grid-cols-12 pt-5">
        <div className="col-start-1 col-end-9">
          <div className="border-[#d9d9d9] border-solid border-[1px] bg-[#eff1f2] rounded-md">
            <div className="mb-4">
              <Collapse defaultActiveKey={["0"]} expandIconPosition={"end"}>
                <Panel
                  header={
                    <>
                      <div className="flex justify-between">
                        <div>
                          <div className="text-[16px] font-medium">
                            {media?.name && media?.name}
                          </div>
                          <div className="flex space-x-1 self-center">
                            {media?.created_by?.first_name && (
                              <div className="sm:flex sm:space-x-3 self-center">
                                <Tooltip title="Media Owner" placement="right">
                                  <CardProfileHeader
                                    user={media?.created_by?.first_name}
                                  />
                                </Tooltip>
                              </div>
                            )}
                            {media?.created_at && (
                              <div className="flex space-x-1">
                                <span>{"|"}</span>
                                <span className="self-center">
                                  <MdOutlineUpdate size={15} />
                                </span>
                                <span className="self-center">
                                  <LocalTime timeVal={media?.created_at} />
                                </span>
                              </div>
                            )}
                          </div>
                        </div>

                        {auth && (
                          <div
                            className="-mt-[0.5rem]"
                            onClick={(e) => handleChildElementClick(e)}
                          >
                            <AntDropDown
                              position="relative"
                              top="0"
                              right="2rem"
                              dropIcon={"VerticalDots"}
                              modelOpen={openModel}
                              value={[
                                { id: "edit", displayName: "Edit" },
                                { id: "copy", displayName: "Copy" },
                                { id: "delete", displayName: "Delete" },
                                { id: "export", displayName: "Export" },
                              ]}
                            />
                          </div>
                        )}
                      </div>
                    </>
                  }
                  key="1"
                >
                  <div className="">
                    {media?.description && (
                      <Paragraphs
                        isExpand={true}
                        rows={6}
                        text={media?.description}
                      />
                    )}
                    {media?.extra_group_response &&
                      media?.extra_group_response.map((custom, i) => (
                        <Descriptions title="">
                          <Descriptions.Item label={custom?.question}>
                            {custom?.response}
                          </Descriptions.Item>
                        </Descriptions>
                      ))}
                    {media?.tags && (
                      <div className="flex w-full">
                        <div className="flex overflow-hidden hover:overflow-x-scroll h-12 ">
                          {media?.tags?.map(
                            (tag, i) =>
                              tag.name !== "" &&
                              tag.count !== 0 && (
                                <div
                                  className="inline-flex p-1 cursor-pointer"
                                  key={i}
                                  onClick={() => {
                                    navigate(`/search/tag=${tag.name}`);
                                  }}
                                >
                                  <TagButton
                                    color="#55acee"
                                    tagName={tag.name}
                                  />
                                </div>
                              )
                          )}
                        </div>
                      </div>
                    )}
                  </div>
                </Panel>
              </Collapse>
            </div>
            <Media updateMedia={updateAnnotate} />
          </div>
        </div>
        <div className="sm:col-start-9 sm:col-span-12 mt-2 sm:mt-0">
          <Collapse defaultActiveKey={["0"]} expandIconPosition={"end"}>
            <Panel
              header={
                <>
                  <div className="text-md flex justify-between items-center">
                    <h3>Annotations</h3>
                    <div className="flex space-x-4">
                      {annotations.length !== 0 && (
                        <span className="mt-[-0.3rem]">
                          <Space direction="vertical">
                            <Space wrap>
                              <Tooltip title="Search annotations">
                                <Button
                                  type="dashed"
                                  shape="circle"
                                  icon={<SearchOutlined />}
                                  onClick={() => {
                                    setSearch(!search);
                                  }}
                                />
                              </Tooltip>
                            </Space>
                          </Space>
                        </span>
                      )}
                    </div>
                  </div>
                </>
              }
              key="1"
            >
              {annotations.length !== 0 && (
                <Space direction="vertical">
                  <Space wrap>
                    <Select
                      defaultValue={annoSort}
                      style={{ width: 200 }}
                      className="w-min"
                      onChange={(e) => {
                        setSortAnno(e);
                      }}
                      options={[
                        {
                          label: "Sort",
                          options: [
                            { label: "Last Modified", value: "last" },
                            { label: "Time Stamp", value: "time" },
                          ],
                        },
                      ]}
                    />
                    {auth && (
                      <Toggle
                        onClick={() => {
                          setViewMyAnno(!viewMyAnno);
                        }}
                        checked={viewMyAnno}
                        title={"My Annotations"}
                      />
                    )}
                  </Space>
                </Space>
              )}
            </Panel>
          </Collapse>
          <div className="p-2 pb-56 h-[43rem] overflow-y-auto scrollbar-hide hover:scrollbar-auto">
            <Annotation
              data={annotations}
              updateAnnotate={updateAnnotate}
              isSearch={search}
              mediaName={media?.name && media?.name}
              group={grp}
              loading={loading}
            />
          </div>
        </div>
      </div>
      {editModal && (
        <DialogModal
          open={editModal}
          onClose={() => {
            setEditModal(false);
          }}
          title={`Edit media  ${media?.name && media?.name}`}
        >
          {error.length !== 0 && (
            <div className="flex items-center justify-center p-5 border-b border-solid border-slate-200 rounded-t">
              <h5 className="text-xl font-medium text-red-600">{error}</h5>
            </div>
          )}
          <EditUploadForm
            onClose={() => {
              setEditModal(false);
            }}
            notify={notifyToast}
            mediaId={media?.uuid && media?.uuid}
            mediaTags={media?.tags && media?.tags}
            mediaName={media?.name && media?.name}
            mediaDescription={media?.description && media?.description}
            groupID={grp?.id}
            onSuccess={updateGroup}
          />
        </DialogModal>
      )}
      {copyModal && (
        <DialogModal
          open={copyModal}
          onClose={() => {
            setCopyModal(false);
            setModelOpen(false);
          }}
          title={"Copy Media to another collection"}
        >
          <CopyModal
            onClose={() => {
              setCopyModal(false);
              setModelOpen(false);
            }}
            mediaId={media?.uuid}
            groupId={grp?.id}
            groups={authDetails?.groups}
            extra={copyExtra}
          />
        </DialogModal>
      )}
      {deleteMediaModal && (
        <AntModel
          // modelTitle={`Are you sure you want to delete this media "${media?.name}" ?`}
          modelTitle={`This action will delete the media and all annotations too. Are you sure you want to delete media created by ${media?.created_by?.first_name} ?`}
          modelOpen={modelOpen}
          updateModel={updateModel}
          okText={"Delete"}
          okType={"danger"}
        />
      )}
    </div>
  );
};

export default AnnotationBlock;
