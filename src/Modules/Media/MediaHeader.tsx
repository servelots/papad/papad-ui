import React from "react";
import { useNavigate } from "react-router-dom";
import { Button } from "../../Components";
import PropTypes from "prop-types";

const MediaHeader = ({
  mediaId,
  title,
  description,
  tag,
  custom,
  createdBy,
  createdDate,
}: {
  mediaId: string;
  title: string;
  description: string;
  tag: any;
  custom?: any;
  createdBy: any;
  createdDate: string;
}) => {
  const navigate = useNavigate();

  return (
    <>
      <div className="relative flex flex-col min-w-0 break-words bg-white rounded mb-16 xl:mb-0 shadow-xl mt-20">
        <div className="flex-auto p-4">
          <div className="flex flex-wrap">
            <div className="relative w-full pr-4 max-w-full flex-grow flex-1">
              <span className="font-semibold text-xl text-blueGray-700">
                {title && title}
              </span>
              <p className="text-blueGray-400 uppercase font-bold text-xs h-auto min-h-24 max-h-28 line-clamp-5 hover:line-clamp-none">
                {description && description}
              </p>
              <div className="pt-2 text-sm text-blueGray-400 mt-auto">
                {custom?.map((ext, i) => (
                  <div
                    className="inline-flex font-base text-base mb-2 px-1"
                    key={i}
                  >
                    <span className="text-xs font-semibold inline-block py-1 px-2 rounded text-blue-500 bg-blue-200 uppercase last:mr-0 mr-1">
                      {ext.question}: {ext.response}
                    </span>
                  </div>
                ))}
              </div>
              <div className="pt-3 gap-2">
                {tag?.map(
                  (tag, i) =>
                    tag.name !== "" &&
                    tag.count !== 0 && (
                      <div
                        className="inline-flex p-1"
                        key={i}
                        onClick={() => {
                          navigate(`/search/tag=${tag.name}`);
                        }}
                      >
                        <span className="text-xs font-semibold inline-block py-1 px-2 rounded-full text-blue-500 bg-blue-200 uppercase last:mr-0 mr-1">
                          {tag.name}
                        </span>
                      </div>
                    )
                )}
              </div>
            </div>
          </div>
        </div>

        <div className="p-6 text-sm text-blueGray-400 mt-auto space-x-10 text-blueGray-400 uppercase font-bold h-auto min-h-24 max-h-28 line-clamp-5 hover:line-clamp-none">
          <span className="mr-2">{createdBy?.first_name}</span>
          <span className="mr-2">
            {createdDate?.split("T").reverse().pop()}
          </span>
        </div>
      </div>
    </>
  );
};

export default MediaHeader;

MediaHeader.defaultProps = {
  mediaId: "",
  title: "Media",
  description: "",
  tag: "",
  custom: "",
  createdBy: "",
  createdDate: "",
};

MediaHeader.propTypes = {
  mediaId: PropTypes.number,
  title: PropTypes.string,
  description: PropTypes.string,
  tag: PropTypes.any,
  custom: PropTypes.any,
  createdBy: PropTypes.any,
  createdDate: PropTypes.string,
};
