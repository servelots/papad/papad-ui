import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { useRecoilState } from "recoil";
import { Loader, Player } from "../../Components";
import {
  startTimer,
  endTimer,
  cardStartTimeState,
  cardEndTimeState,
  editAnnoIDState,
  deleteAnnoIDState,
  currentTimer,
  durationTimer,
} from "../AuthState/AuthState";
import {
  ReloadOutlined,
  FileImageOutlined,
  SettingOutlined,
} from "@ant-design/icons";
import {
  Badge,
  Form,
  InputNumber,
  Modal,
  notification,
  Button,
  Upload,
  Space,
  Select,
  Divider,
} from "antd";
import AntButton from "antd/es/button";
import Tags from "./Tags";
import dayjs from "dayjs";
import customParseFormat from "dayjs/plugin/customParseFormat";
import { secToTimestamp } from "./Annotation";
import videojs from "video.js";
import type { RcFile, UploadFile, UploadProps } from "antd/es/upload/interface";
import TextEditor from "./TextEditor";
import { MdAdd } from "react-icons/md";

dayjs.extend(customParseFormat);
type NotificationType = "success" | "info" | "warning" | "error";

const timestampToSec = (timestamp: string): number => {
  const [hours, minutes, seconds] = timestamp.split(":").map(Number);
  const totalSeconds = hours * 3600 + minutes * 60 + seconds;
  return totalSeconds;
};

const Media = ({ updateMedia }) => {
  const [upload, setUpload] = useState<string>("");
  const [uploadType, setUploadType] = useState<string>("");
  const [grpId, setGrpId] = useState("");
  const [error, setError] = useState<any>([]);
  const [loading, setLoading] = useState(true);
  const [media, setMedia] = useState<any>([]);
  const [start, setStart] = useState<any>(-1);
  const [end, setEnd] = useState<any>(-1);
  const { id } = useParams();
  const [uuid, setUuid] = useState("");
  const navigate = useNavigate();
  const auth = localStorage.getItem("token");
  const baseURL = localStorage.getItem("prod_url");
  const [durationEdit, setDurationEdit] = useState(false);
  const [annotations, setAnnotations] = useState<any>([]);
  const [startTime, setStartTime] = useRecoilState<any>(startTimer);
  const [endTime, setEndTime] = useRecoilState<any>(endTimer);
  const [cardStartTime, setCardStartTime] =
    useRecoilState<any>(cardStartTimeState);
  const [cardEndTime, setCardEndTime] = useRecoilState<any>(cardEndTimeState);
  const [tagList, setTagList] = useState([]);
  const [fileList, setFileList] = useState<UploadFile[]>([]);
  const [previewOpen, setPreviewOpen] = useState(false);
  const [previewImage, setPreviewImage] = useState("");
  const [previewTitle, setPreviewTitle] = useState("");
  const [annoModal, setAnnoModal] = useState(false);
  const [groupData, setGroupData] = useState<any>([]);
  const userUUID = localStorage.getItem("userUUID");
  const [editAnnotation, setEditAnnotation] = useRecoilState(editAnnoIDState);
  const [deleteAnnotation, setdeleteAnnotation] =
    useRecoilState(deleteAnnoIDState);
  const [tags, setTags] = useState<any>([]);
  const [editAnnoText, setEditAnnoText] = useState<string>("");
  const [editAnnoImg, setEditAnnoImg] = useState<any>([]);
  const [isEditAnno, setIsEditAnno] = useState(false);
  const [currentTime, setCurrentTime] = useRecoilState<any>(currentTimer);
  const [inputStartTime, setInputStartTime] = useState<any>(0);
  const [inputEndTime, setInputEndTime] = useState(0);
  const [durationTime, setDurationTime] = useRecoilState(durationTimer);
  const [durationInterval, setDurationInterval] = useState(0);
  const [allDurations, setAllDurations] = useState<any>([]);

  const { Option } = Select;

  const [form] = Form.useForm<{
    text: string;
    tags: [];
    startTime: string;
    endTime: string;
    uploadImage: [];
  }>();
  const [api, contextHolder] = notification.useNotification();

  const openNotificationWithIcon = (type: NotificationType, msg) => {
    api[type]({
      message: msg,
      placement: "topRight",
    });
  };

  const onChange = (key: string | string[]) => {
    console.log(key);
  };

  const onDurationChange = () => {
    setDurationEdit(!durationEdit);
  };

  const genExtra = () => (
    <SettingOutlined
      onClick={(event) => {
        // If you don't want click extra trigger collapse, you can prevent this:
        event.stopPropagation();
      }}
    />
  );

  const onReset = () => {
    //
  };

  useEffect(() => {
    if (id !== undefined) {
      setUuid(id);
    }
    // if (editAnnotation !== "") {
    //   setEditAnnotation("");
    //   navigate(window.location.pathname.split("/annotation")[0]);
    // }
    getUpload();
    getAnnotations();
  }, []);

  useEffect(() => {
    console.log(deleteAnnotation && onCancelForm());
  }, [deleteAnnoIDState]);

  useEffect(() => {
    setFileList(editAnnoImg);
    form.setFieldValue("uploadImage", [editAnnoImg]);
  }, [editAnnoImg.length !== 0]);

  useEffect(() => {
    if (cardStartTime !== "") {
      const seconds: number = timestampToSec(cardStartTime);
      handleVideoPlayer("cardStartTime", seconds);
    }
  }, [cardStartTime]);

  useEffect(() => {
    if (cardEndTime !== "") {
      if (cardEndTime === secToTimestamp(currentTime)) {
        handleVideoPlayer("cardEndTime", cardEndTime);
      }
    }
  }, [currentTime]);

  useEffect(() => {
    form.setFieldValue("startTime", dayjs(startTime, "HH:mm:ss"));
    form.setFieldValue("endTime", dayjs(endTime, "HH:mm:ss"));
  }, [startTimer, startTime, endTime]);

  useEffect(() => {
    if (editAnnotation !== "") {
      setTagList([]);
      setTags([]);
      getAnnotationByID(editAnnotation);
      setAnnoModal(true);
    }
  }, [editAnnotation]);

  useEffect(() => {
    let updatedTag = [];
    if (tags.length !== 0) {
      tags
        .filter((tag) => tag.name !== "")
        .map((tag) => {
          updatedTag.push(tag.name);
        });
      setTagList(updatedTag);
      form.setFieldValue("tags", updatedTag);
    }
  }, [tags]);

  const getDuration = () => {
    if (upload !== null) {
      console.log("Duration: ", durationTime);
    }
  };

  const getUpload = () => {
    const options = {
      method: "GET",
      url: `${baseURL}api/v1/archive/${id}/`,
    };

    axios
      .request(options)
      .then((response) => {
        setUpload(response.data.upload);
        setUploadType(videoType(response.data?.upload?.split(".").pop()));
        setGrpId(response.data.group.id);
        setMedia(response.data);
        setGroupData(response.data.group);
        setLoading(false);
      })
      .catch((e) => {
        console.log(e);
        setLoading(false);
      });
  };

  const getAnnotations = () => {
    const options = {
      method: "GET",
      url: `${baseURL}api/v1/archive/${id}/`,
    };

    axios
      .request(options)
      .then((response) => {
        setGrpId(response.data.group.id);
        setAnnotations(response.data.annotations);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const getAnnotationByID = (id) => {
    setIsEditAnno(true);
    const options = {
      method: "GET",
      url: `${baseURL}api/v1/annotate/${id}/`,
    };

    axios
      .request(options)
      .then((response) => {
        setAnnotations(response?.data);
        setStartTime(
          secToTimestamp(
            response?.data.media_target.split("=")[1].split(",")[0]
          )
        );
        setEndTime(
          secToTimestamp(
            response?.data.media_target.split("=")[1].split(",")[1]
          )
        );
        setStart(response?.data.media_target.split("=")[1].split(",")[0]);
        setEnd(response?.data.media_target.split("=")[1].split(",")[1]);
        setTags(response.data.tags);
        form.setFieldValue("text", response?.data.annotation_text);
        setEditAnnoText(response?.data.annotation_text);
        const updateFileList = {
          uid: uuid,
          name: "image.png",
          status: "done",
          url: response?.data.annotation_image,
        };
        setEditAnnoImg(updateFileList);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const handleExport = (id, exporting) => {
    const options = {
      method: "POST",
      url: `${baseURL}api/v1/export/`,
      headers: {
        Authorization: `Token ${auth}`,
        "Content-Type": "application/json",
      },
      data: { type: exporting, id: id },
    };

    axios
      .request(options)
      .then((response) => {
        navigate("/requests");
      })
      .catch((error) => {
        console.error(error);
        setError(error.response.data);
      });
  };

  const updateAnnotate = () => {
    getAnnotations();
  };

  const onFinish = (values: any) => {
    setLoading(true);
    let newList = values.uploadImage;
    !isEditAnno && newList.reverse();
    console.log("Success:", values);
    const payload = new FormData();
    !isEditAnno && payload.append("media_reference_id", uuid);
    payload.append("annotation_text", values.text);
    payload.append(
      "media_target",
      `t=${
        values.startTime["$H"] * 60 * 60 +
        values.startTime["$m"] * 60 +
        values.startTime["$s"]
      },${
        values.endTime["$H"] * 60 * 60 +
        values.endTime["$m"] * 60 +
        values.endTime["$s"]
      }`
    );
    payload.append("tags", tagList.join(","));
    !isEditAnno && payload.append("annotation_image", newList[0]);

    const options = {
      method: `${isEditAnno ? "PUT" : "POST"}`,
      url: `${baseURL}api/v1/annotate/${
        isEditAnno ? `${editAnnotation}/` : ""
      }`,
      headers: {
        "Content-Type": "multipart/form-data",
        Authorization: `Token ${auth}`,
      },
      data: payload,
    };

    axios
      .request(options)
      .then((data) => {
        updateMedia(data);
        openNotificationWithIcon("success", "Successfully Uploaded Annotation");
        localStorage.removeItem("startTime");
        localStorage.removeItem("endTime");
        onCancelForm();
        setLoading(false);
      })
      .catch((error) => {
        // updateMedia(payload);
        openNotificationWithIcon("error", "Failed to upload Annotation");
        console.error(error);
        setLoading(false);
      });
  };
  const onCancelForm = () => {
    setAnnoModal(false);
    setIsEditAnno(false);
    form.resetFields();
    setStart(-1);
    setEnd(-1);
    // videojs.getPlayer("videojs")?.currentTime(0);
    setFileList([]);
    setTagList([]);
    setEditAnnoText("");
    setEditAnnotation("");
    navigate(window.location.pathname.split("/annotation")[0]);
  };
  const onFinishFailed = (errorInfo: any) => {
    console.log("Failed:", errorInfo);
  };
  const updateTagList = (inputValue: any) => {
    setTagList(inputValue.flat());
  };

  const handleVideoPlayer = (e?: any, specificTime?: number) => {
    if (videojs.getPlayer("videojs")) {
      const player = videojs.getPlayer("videojs");

      switch (e) {
        case "start":
          setStart(player.currentTime());
          localStorage.setItem("startTime", player.currentTime().toString());
          form.setFieldValue(
            "startTime",
            dayjs(secToTimestamp(player.currentTime().toString()), "HH:mm:ss")
          );
          player.play();
          break;
        case "end":
          setEnd(player.currentTime());
          form.setFieldValue(
            "endTime",
            dayjs(secToTimestamp(player.currentTime().toString()), "HH:mm:ss")
          );
          localStorage.setItem("endTime", player.currentTime().toString());

          if (start === -1) {
            setStart(0);
            form.setFieldValue("startTime", dayjs("00:00:00", "HH:mm:ss"));
            localStorage.setItem("startTime", "0");
          }
          player.pause();
          break;
        case "refresh":
          if (player) {
            player.play();
            player.currentTime(end);
          }
          break;
        case "cardStartTime":
          if (player) {
            player.currentTime(specificTime);
            player.play();
            setCardStartTime("");
          }
          break;
        case "cardEndTime":
          if (player) {
            player.pause();
            setCardEndTime("");
          }
          break;
      }
    }
  };

  const handleUpload = (file) => {
    setUpload(file[0]);
    form.setFieldValue("uploadImage", file[0]);
  };

  const getBase64 = (file: RcFile): Promise<string> =>
    new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result as string);
      reader.onerror = (error) => reject(error);
    });

  const props: UploadProps = {
    onRemove: (file) => {
      const index = fileList.indexOf(file);
      const newFileList = fileList.slice();
      newFileList.splice(index, 1);
      form.setFieldValue("", newFileList);
      setFileList(newFileList);
      form.setFieldValue("uploadImage", newFileList);
    },
    beforeUpload: (file) => {
      setFileList([...fileList, file]);
      form.setFieldValue("uploadImage", [...fileList, file]);
      return false;
    },
    fileList,
    showUploadList: {
      showRemoveIcon: false,
    },
  };

  const handleCancel = () => setPreviewOpen(false);

  const handlePreview = async (file) => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj);
    }
    setPreviewImage(file.url || file.preview);
    setPreviewOpen(true);
    setPreviewTitle(
      file.name || file.url.substring(file.url.lastIndexOf("/") + 1)
    );
  };
  const handleChange = ({ fileList: newFileList }) => {
    setFileList(newFileList);
  };
  const updateTextArea = (value) => {
    if (value === "<p><br></p>") {
      form.setFieldValue("text", null);
    } else {
      form.setFieldValue("text", value);
    }
  };

  const videoType = (src) => {
    switch (src) {
      case "mp4":
        return "video/mp4";
      case "mpeg4":
        return "video/mpeg4";
      case "mkv":
        return "video/mkv";
      case "m4v":
        return "video/m4v";
      case "avi":
        return "video/avi";
      case "flv":
        return "video/flv";
      case "ogv":
        return "video/ogv";
      case "flv":
        return "video/flv";
      case "gif":
        return "video/gif";
      case "gifv":
        return "video/gifv";
      case "mov":
        return "video/mov";
      case "wmv":
        return "video/wmv";
      case "asf":
        return "video/asf";
      case "mpeg":
        return "video/mpeg";

      case "mp3":
        return "audio/mp3";
      case "amr":
        return "audio/AMR";
      case "aac":
        return "audio/aac";
      case "m4a":
        return "audio/m4a";
      case "opus":
        return "audio/ogg";
      case "wav":
        return "audio/wav";
      case "ogg":
        return "audio/ogg";
      case "wma":
        return "audio/wma";
      case "3gp":
        return "audio/3gp";
      case "aax":
        return "audio/aax";
      case "aa":
        return "audio/aa";
      case "act":
        return "audio/act";
      case "flac":
        return "audio/flac";
      case "dvf":
        return "audio/dvf";
      case "m4b":
        return "audio/m4b";
      case "m4p":
        return "audio/m4p";
      case "mpc":
        return "audio/mpc";
      case "msv":
        return "audio/msv";
      case "nmf":
        return "audio/nmf";
      case "oga":
        return "audio/oga";
      case "mogg":
        return "audio/mogg";
      case "raw":
        return "audio/raw";
      case "sln":
        return "audio/sln";
      case "tta":
        return "audio/tta";
      case "voc":
        return "audio/voc";
      case "vox":
        return "audio/vox";
      case "webm":
        return "audio/webm";
      case "cda":
        return "audio/cda";
    }
  };

  if (loading) {
    return <Loader />;
  }

  const DraggableUploadListItem = ({ file }) => {
    // prevent preview event when drag end

    const index = fileList.indexOf(file);
    const newFileList = fileList.slice();
    newFileList.splice(index, 1);
    form.setFieldValue("", newFileList);
    setFileList(newFileList);
    form.setFieldValue("uploadImage", newFileList);
  };

  const handleSetStartTime = (value) => {
    if (value !== undefined) {
      if (videojs.getPlayer("videojs")) {
        const player = videojs.getPlayer("videojs");
        setStart(timestampToSec(value));
        localStorage.setItem("startTime", timestampToSec(value).toString());
        form.setFieldValue(
          "startTime",
          dayjs(secToTimestamp(timestampToSec(value).toString()), "HH:mm:ss")
        );
        player.currentTime(timestampToSec(value));
        player.play();
      }
    }
  };

  const handleSetEndTime = (value) => {
    if (value !== undefined) {
      if (videojs.getPlayer("videojs")) {
        const player = videojs.getPlayer("videojs");
        setEnd(timestampToSec(value));
        form.setFieldValue(
          "endTime",
          dayjs(secToTimestamp(timestampToSec(value).toString()), "HH:mm:ss")
        );
        localStorage.setItem("endTime", timestampToSec(value).toString());

        if (start === -1) {
          setStart(0);
          form.setFieldValue("startTime", dayjs("00:00:00", "HH:mm:ss"));
          localStorage.setItem("startTime", "0");
        }
        player.currentTime(timestampToSec(value));
        player.pause();
      }
    }
  };

  const splitDuration = (duration: number, interval: number): number[] => {
    const parts = Math.ceil(duration / interval);

    return Array.from({ length: parts }, (_, index) => {
      const start = index * interval;
      const end = Math.min(start + interval, duration);
      return end - start;
    });
  };

  return (
    <>
      {contextHolder}
      <div className="">
        <Form
          form={form}
          initialValues={{
            startTime: dayjs(startTime, "HH:mm:ss"),
            endTime: dayjs(endTime, "HH:mm:ss"),
            tags: tagList,
            uploadImage: fileList,
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
          layout="vertical"
        >
          <div className="p-2 overflow-hidden items-center h-[40rem] max-h-full">
            {upload !== null && (
              <div
                className="flex items-end justify-end"
                onClick={() => window.open(upload, "_blank")}
              >
                <Badge
                  count={uploadType}
                  showZero
                  color={"#faad14"}
                  // className="ml-4 mt-[-0.3rem]"
                />
              </div>
            )}
            <div
              className={`flex sm:flex md:flex md:gap-2 sm:gap-2  ${
                annoModal ? "place-content-start" : "place-content-center"
              }`}
            >
              {upload !== null ? (
                <Player upload={upload} type={uploadType} />
              ) : (
                // <VideoPlayer src={upload} type={uploadType} />
                <div className=" flex items-center justify-center">
                  Media is not available
                </div>
              )}
              {annoModal && (
                <div className="max-w-[22rem]">
                  <div className="flex item-center justify-center my-2">
                    <div className="text-gray-700 text-3xl">
                      {secToTimestamp(currentTime)}
                    </div>
                  </div>
                  <div
                    className={`flex gap-8 sm:gap-10 flex-col items-center justify-center`}
                  >
                    <Form.Item
                      name="startTime"
                      label="Start time"
                      rules={[
                        {
                          required: true,
                          message: "Please select start time.",
                        },
                      ]}
                    >
                      <div className="flex flex-col">
                        <Button
                          type="primary"
                          id="start"
                          onClick={() => {
                            handleVideoPlayer("start");
                            getDuration();
                          }}
                          style={{ width: 200 }}
                        >
                          {start === -1
                            ? "Set Start time"
                            : secToTimestamp(start)}
                        </Button>

                        {parseInt(durationTime) !== 0 && (
                          <div className="flex justify-center items-center mt-4">
                            <Select
                              style={{ width: 200 }}
                              placeholder="Select time by seconds"
                              autoClearSearchValue={false}
                              removeIcon={true}
                              onChange={handleSetStartTime}
                              dropdownRender={(menu) => (
                                <>
                                  <Space
                                    className="flex flex-col"
                                    style={{ padding: "0 2px 1px" }}
                                  >
                                    <span>Add Interval</span>
                                    <Space>
                                      <InputNumber
                                        placeholder="Add new tag"
                                        style={{ width: 75 }}
                                        value={durationInterval}
                                        onChange={(e) => {
                                          setDurationInterval(e);
                                        }}
                                      />
                                      <Button
                                        type="primary"
                                        icon={<MdAdd className="pt-1" />}
                                        onClick={() => {
                                          setAllDurations(
                                            splitDuration(
                                              parseInt(durationTime),
                                              durationInterval
                                            )
                                          );
                                        }}
                                        className="flex"
                                      >
                                        Add item
                                      </Button>
                                    </Space>
                                  </Space>
                                  <Divider style={{ margin: "8px 0" }} />
                                  {menu}
                                </>
                              )}
                            >
                              {allDurations.length !== 0 &&
                                allDurations?.map((item, key) => (
                                  <Option
                                    value={secToTimestamp(item * key)}
                                    label={secToTimestamp(item * key)}
                                    key={key}
                                  >
                                    <Space>{secToTimestamp(item * key)}</Space>
                                  </Option>
                                ))}
                            </Select>
                          </div>
                        )}
                      </div>
                    </Form.Item>

                    <Form.Item
                      name="endTime"
                      label="End time"
                      rules={[
                        {
                          required: true,
                          message: "Please select send time.",
                        },
                      ]}
                    >
                      <div className="flex flex-col">
                        <Button
                          type="primary"
                          id="end"
                          onClick={() => {
                            handleVideoPlayer("end");
                            getDuration();
                          }}
                          style={{ width: 200 }}
                        >
                          {end === -1 ? "Set End time" : secToTimestamp(end)}
                        </Button>
                        {parseInt(durationTime) !== 0 && (
                          <div className="flex justify-center items-center mt-4">
                            <Select
                              style={{ width: 200 }}
                              placeholder="Select time by seconds"
                              autoClearSearchValue={false}
                              removeIcon={true}
                              onChange={handleSetEndTime}
                              dropdownRender={(menu) => (
                                <>
                                  <Space
                                    className="flex flex-col"
                                    style={{ padding: "0 2px 1px" }}
                                  >
                                    <span>Add Interval</span>
                                    <Space>
                                      <InputNumber
                                        placeholder="Add new tag"
                                        style={{ width: 75 }}
                                        value={durationInterval}
                                        onChange={(e) => {
                                          setDurationInterval(e);
                                        }}
                                      />
                                      <Button
                                        type="primary"
                                        icon={<MdAdd className="pt-1" />}
                                        style={{ width: 75 }}
                                        onClick={() => {
                                          setAllDurations(
                                            splitDuration(
                                              parseInt(durationTime),
                                              durationInterval
                                            )
                                          );
                                        }}
                                        className="flex"
                                      >
                                        Add item
                                      </Button>
                                    </Space>
                                  </Space>
                                  <Divider style={{ margin: "8px 0" }} />
                                  {menu}
                                </>
                              )}
                            >
                              {allDurations.length !== 0 &&
                                allDurations?.map((item, key) => (
                                  <Option
                                    value={secToTimestamp(item * key)}
                                    label={secToTimestamp(item * key)}
                                    key={key}
                                  >
                                    <Space>{secToTimestamp(item * key)}</Space>
                                  </Option>
                                ))}
                            </Select>
                          </div>
                        )}
                      </div>
                    </Form.Item>
                  </div>

                  <Form.Item name="tags" label={`Tags`}>
                    <Tags updateTags={updateTagList} tagList={tagList} />
                  </Form.Item>
                  {/* <Checkbox checked={durationEdit === true ? false : true } onChange={onDurationChange} className="w-[6rem]">Edit Time</Checkbox> */}
                </div>
              )}
            </div>

            {/* {auth &&
              !annoModal &&
              groupData.users?.map((user, i) => {
                return (
                  user.id.includes(userUUID) && (
                    <div className="flex items-center justify-center pt-3">
                      <Button
                        id="annotate"
                        size="sm"
                        type="secondary"
                        onClick={() => setAnnoModal(true)}
                        className="flex item-center justify-center disabled:opacity-25 w-[10rem] py-2"
                        key={i}
                      >
                        Annotate
                      </Button>
                    </div>
                  )
                );
              })} */}

            {!annoModal &&
              auth &&
              upload !== null &&
              groupData.users?.map((user, i) => {
                return (
                  user.id.includes(userUUID) && (
                    <div
                      className="flex items-center justify-center pt-3"
                      key={i}
                    >
                      <Space direction="vertical" key={i}>
                        <Space wrap>
                          <Button
                            type="primary"
                            size={"large"}
                            onClick={() => {
                              setAnnoModal(true);
                              getDuration();
                            }}
                          >
                            Annotate
                          </Button>
                        </Space>
                      </Space>
                    </div>
                  )
                );
              })}
          </div>
          {annoModal && (
            <>
              {/* <hr className="border border-t-[1px] border-solid border-slate-200 my-2"/> */}

              <div className="p-2">
                <div className="">
                  <Form.Item
                    name="text"
                    label="Annotate Text"
                    rules={[
                      {
                        required: true,
                        message: "Please input your text content.",
                      },
                    ]}
                  >
                    {/* <Input.TextArea rows={10} /> */}
                    <TextEditor
                      updateTextFeild={updateTextArea}
                      editValue={editAnnoText ? editAnnoText : ""}
                    />
                  </Form.Item>
                  <div className="sm:flex justify-between gap-5">
                    {isEditAnno ? null : (
                      <Form.Item
                        name="uploadImage"
                        label="Upload Image"
                        noStyle
                      >
                        <div className="h-fit flex gap-2">
                          <Upload
                            multiple={false}
                            maxCount={1}
                            name="files"
                            {...props}
                            listType="picture-card"
                            fileList={fileList}
                            onPreview={handlePreview}
                            onChange={handleChange}
                            accept=".png,.jpg,.jpeg,.svg,.gif,.xcf"
                          >
                            <div className="flex gap-2 px-1 sm:w-[25rem]">
                              {fileList?.length !== 0 ? (
                                <>
                                  <p className="ant-upload-drag-icon">
                                    <ReloadOutlined
                                      style={{ fontSize: "28px" }}
                                    />
                                  </p>
                                  <p className="ant-upload-text self-center">
                                    Delete & Upload Again
                                  </p>
                                </>
                              ) : (
                                <>
                                  <p className="ant-upload-drag-icon">
                                    <FileImageOutlined
                                      style={{ fontSize: "28px" }}
                                    />
                                  </p>
                                  <p className="ant-upload-text self-center">
                                    Upload or Drag
                                  </p>
                                </>
                              )}
                            </div>
                          </Upload>
                        </div>
                        <Modal
                          open={previewOpen}
                          title={previewTitle}
                          footer={null}
                          onCancel={handleCancel}
                          {...props}
                        >
                          <img
                            alt=""
                            style={{ width: "100%" }}
                            src={previewImage}
                          />
                          <div
                            className="flex place-content-center justify-center items-center pt-2"
                            onClick={() => {
                              setFileList([]);
                              handleCancel();
                            }}
                          >
                            <AntButton
                              className="bg-red-500 text-white"
                              type="default"
                              size="middle"
                              htmlType="submit"
                            >
                              Delete
                            </AntButton>
                          </div>
                        </Modal>
                      </Form.Item>
                    )}

                    <div className="self-end mt-6 sm:mt-0">
                      <Form.Item>
                        <div className="flex gap-2">
                          <AntButton
                            className="bg-[#108ee9]"
                            type="primary"
                            size="middle"
                            htmlType="submit"
                            // disabled={end === -1 || start > end}
                            disabled={
                              (end === -1 && true) ||
                              (start > end && true) ||
                              (start === end && true) ||
                              loading
                            }
                          >
                            {isEditAnno ? "Submit" : "Create"}
                          </AntButton>
                          <AntButton
                            type="text"
                            size="middle"
                            onClick={() => {
                              onCancelForm();
                            }}
                          >
                            Cancel
                          </AntButton>
                        </div>
                      </Form.Item>
                      <div></div>
                    </div>
                  </div>
                </div>
              </div>
            </>
          )}
        </Form>
      </div>
    </>
  );
};

export default Media;
