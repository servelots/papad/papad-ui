import { Cascader, Col, Input, Row, Select, Space } from "antd";
import axios from "axios";
import React, { useCallback, useEffect, useRef, useState } from "react";
import { MdClose, MdOutlineReportProblem, MdSearch } from "react-icons/md";
import { useNavigate, useParams } from "react-router-dom";
import { useRecoilState } from "recoil";
import {
  AnnotationCard,
  CustomQuestionModal,
  DeleteModal,
  DialogModal,
  IconButton,
  Loader,
} from "../../Components";
import CardLayout from "../../Components/Cards/CardLayout";
import { deleteAnnoIDState } from "../AuthState/AuthState";
import { secToTimestamp } from "../Media/Annotation";
import { message } from "antd";
import copy from "copy-to-clipboard";
import AntModel from "../../Components/Modal/AntModel";

const Search = () => {
  const searchInput = useRef<HTMLInputElement>(null);
  const [isShown, setIsShown] = useState(false);
  const [groupToggle, setGroupToggle] = useState(false);
  const [curMedia, setCurMedia] = useState<string>("");
  const [mediaVal, setMediaVal] = useState("search");
  const [annoVal, setAnnoVal] = useState("name");
  const [disMedia, setDisMedia] = useState(true);
  const [multiSearch, setMultiSearch] = useState(false);
  const [multiGrpVal, setMultiGrpVal] = useState("");
  const [multiGrpData, setMultiGrpData] = useState("");
  const [multi, setMulti] = useState<string>("");
  const [me, setMe] = useState<any[]>([]);
  const [searchMedia, setSearchMedia] = useState<any[]>([]);
  const [searchAnno, setSearchAnno] = useState<any[]>([]);
  const [customQuestionModal, setCustomQuestionModal] = useState(false);
  const [customQues, setCustomQues] = useState(false);
  const [extraId, setExtraId] = useState("");
  const [openTab, setOpenTab] = useState(1);
  const [openKey, setOpenKey] = useState(null);
  const [openGroup, setOpenGroup] = useState(3);
  const [multiGroup, setMultiGroup] = useState<any>([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState<string>("");
  const [shareId, setShareId] = useState("");
  const [modelOpen, setModelOpen] = useState(false);
  const [annoId, setAnnoId] = useState(null);
  const [deleteModal, setDeleteModal] = useState(false);
  const [deleteAnnotation, setDeleteAnnotation] =
    useRecoilState(deleteAnnoIDState);
  const [exporting, setExporting] = useState("");
  const [showModal, setShowModal] = useState(false);

  const toggle = () => setIsShown(!isShown);
  const { val } = useParams();
  const baseURL = localStorage.getItem("prod_url");
  const auth = localStorage.getItem("token");
  const [messageApi, contextHolder] = message.useMessage();
  const url = window.location.href.split("/");
  const navigate = useNavigate();

  useEffect(() => {
    if (val !== undefined && val !== "") {
      const cur = val?.split("=");
      if (cur[0] !== "tag") {
        setCurMedia(val);
      } else if (cur[0] === "tag") {
        setAnnoVal("tag");
        setCurMedia(cur[1]);
      }
    }
    getUser();
  }, []);

  useEffect(() => {
    if (curMedia !== "") {
      mediaVal === "annotations" ? searchAnnotation() : searchBy();
    }
  }, [curMedia]);

  useEffect(() => {
    me.map((grp) => multiGroup.push({ value: grp.id, label: grp.name }));
  }, [me]);

  const searchAnnotation = () => {
    const options = {
      method: "GET",
      url: `${baseURL}api/v1/annotate/?${annoVal}=${curMedia}`,
      headers: {
        "Content-Type": "multipart/form-data",
      },
    };

    axios
      .request(options)
      .then((response) => {
        setSearchAnno(response.data.results);
      })
      .catch((e) => {
        console.log(e);
        setError(e?.message);
      });
  };

  console.log(
    `${baseURL}api/v1/archive/?${mediaVal}=${curMedia}${multiGrpVal}${multiGrpData}`
  );

  const searchBy = () => {
    const options = {
      method: "GET",
      url: `${baseURL}api/v1/archive/?${mediaVal}=${curMedia}${multiGrpVal}${multiGrpData}`,
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
    };

    const AuthOptions = {
      method: "GET",
      url: `${baseURL}api/v1/archive/?${mediaVal}=${curMedia}${multiGrpVal}${multiGrpData}`,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: `Token ${auth}`,
      },
    };

    const searchCustomQuesOptions = {
      method: "GET",
      url: `${baseURL}api/v1/archive/`,
      params: {
        extra_question_id: extraId,
        extra_question_response: curMedia,
      },
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: `Token ${auth}`,
      },
    };

    axios
      .request(
        auth ? (customQues ? searchCustomQuesOptions : AuthOptions) : options
      )
      .then((response) => {
        setSearchMedia(response.data.results);
      })
      .catch((e) => {
        console.log(e);
        setError(e?.message);
      });
  };

  const getUser = () => {
    const options = {
      method: "GET",
      url: `${baseURL}auth/users/me/`,
      headers: {
        Authorization: `Token ${auth}`,
      },
    };

    axios
      .request(options)
      .then((response) => {
        setMe(response.data.groups);
      })
      .catch((e) => {
        console.log(e);
        setError(e?.message);
      });
  };

  const handleSearch = ({
    target,
  }: React.ChangeEvent<HTMLInputElement>): void => {
    const searchWord: string = target.value;

    searchWord === "*" ? setCurMedia("") : setCurMedia(searchWord);
  };

  const handleFocus = () => {
    if (searchInput && searchInput.current) {
      searchInput.current.focus();
    }
  };

  const handleDropdown = (e) => {
    if (e) {
      switch (e) {
        case "publicCollection":
          setMultiGrpVal("");
          setMultiGrpData("");
          setMultiSearch(false);
          setGroupToggle(!groupToggle);
          break;

        case "myCollection":
          setMultiGrpVal("");
          setMultiGrpData("");
          setMultiSearch(false);
          setGroupToggle(!groupToggle);
          break;

        case "multipleCollection":
          setMultiGrpVal("&group=");
          setMultiSearch(!multiSearch);
          break;

        case "name":
          setMediaVal("name");
          setCurMedia("");
          setDisMedia(true);
          handleFocus();
          break;

        case "description":
          setMediaVal("description");
          setCurMedia("");
          setDisMedia(true);
          handleFocus();
          break;

        case "tag":
          setMediaVal("tag");
          setCurMedia("");
          setDisMedia(true);
          handleFocus();
          break;

        case "annotationName":
          setMediaVal("annotations");
          setAnnoVal("name");
          setCurMedia("");
          setDisMedia(false);
          handleFocus();
          break;

        case "annotationTag":
          setMediaVal("annotations");
          setAnnoVal("tag");
          setCurMedia("");
          setDisMedia(false);
          handleFocus();
          break;

        case "Search by Custom Question/s":
          setCustomQuestionModal(!customQuestionModal);
          setCustomQues(true);
          setMediaVal("");
          setDisMedia(false);
          handleFocus();
          break;

        default:
          setMultiGrpVal("");
          setMediaVal("search");
          setCurMedia("");
          setDisMedia(true);
          handleFocus();
          break;
      }
    }
  };

  const handleMulti = (multi) => {
    setMultiGrpData(multi.join(","));
  };

  const handleSelectAnnotation = (anno) => {
    handleDropdown(anno[1]);
  };

  const handleCustom = (id) => {
    setExtraId(id);
  };

  const searchOf = ["Name", "Description", "Tag"];
  const searchAcross = [
    "Public Collection",
    "My Collection",
    "Multiple Collection",
  ];
  const searchIn = ["Media", "Annotation"];

  const [selectedSearchOf, setSelectedSearchOf] = useState<string[]>([]);
  const [selectedSearchAcross, setSelectedSearchAcross] = useState<string[]>(
    []
  );
  const [selectedSearchIn, setSelectedSearchIn] = useState<string[]>([]);

  const displayRender = (labels: string[]) => labels[labels.length - 1];

  const filteredSearchOf = searchOf.filter(
    (o) => !selectedSearchOf.includes(o)
  );
  const filteredSearchAcross = searchAcross.filter(
    (o) => !selectedSearchAcross.includes(o)
  );
  const filteredSearchIn = searchIn.filter(
    (o) => !selectedSearchIn.includes(o)
  );

  const success = () => {
    messageApi.open({
      type: "success",
      content: "Copied the annoation id",
      duration: 10,
    });
  };

  const handleCopyText = (value) => {
    copy(value);
    success();
    // alert(`Copied to clipboard : "${value}"`);
  };

  const shareModal = useCallback(
    (val) => {
      setShareId(val);
      setModelOpen(true);
    },
    [showModal]
  );

  const deleteAnnoModal = useCallback(
    (val) => {
      setAnnoId(val);
      console.log(val);
      setDeleteModal(true);
    },
    [deleteModal]
  );

  const exportAnnotation = useCallback(
    (val) => {
      setAnnoId(val);
      setExporting("annotation");
      handleExport(val, "annotation");
    },
    [exporting]
  );

  const updateModel = (value) => {
    if (value === "ok") {
      handleCopyText(
        url[0] +
          "/" +
          url[2] +
          "/" +
          url[3] +
          "/" +
          url[4] +
          "/" +
          url[5] +
          "/" +
          url[6] +
          `/annotation/${shareId}`
      );
      setModelOpen(false);
    } else {
      setModelOpen(false);
    }
  };

  const handleAnnotation = (annoId) => {
    const options = {
      method: "DELETE",
      url: `${baseURL}api/v1/annotate/${annoId}/`,
      headers: {
        Authorization: `Token ${auth}`,
      },
    };

    axios
      .request(options)
      .then((response) => {
        console.log(response);
        setDeleteAnnotation("true");
      })
      .catch((e) => {
        console.log(e);
        setError(e.response.data.detail);
      });
  };

  const handleExport = (id, exporting) => {
    const options = {
      method: "POST",
      url: `${baseURL}api/v1/export/`,
      headers: {
        Authorization: `Token ${auth}`,
        "Content-Type": "application/json",
      },
      data: { type: exporting, id: id },
    };

    axios
      .request(options)
      .then((response) => {
        console.log(response.data);
        navigate("/requests");
      })
      .catch((error) => {
        console.error(error);
        setError(error.response.data);
      });
  };

  if (error) {
    return <Loader error={error} />;
  }

  if (loading) {
    return <Loader />;
  }

  return (
    <>
      <div className="container ">
        <div className="grid sm:space-y-0 sm:grid sm:grid-cols-1 lg:grid-cols-8 pt-10">
          <div></div>
          <div className="col-span-6">
            <div className="md:w-auto w-full p-3 bg-white rounded-full">
              <div className="grid lg:grid-cols-6 sm:grid-cols-1 md:grid-cols-2">
                <div className="col-span-2 flex">
                  <IconButton
                    size="xs"
                    type="tertiary"
                    className="pt-2"
                    color="purple"
                  >
                    <MdSearch size={30} />
                  </IconButton>
                  <Input
                    type="text"
                    // ref={searchInput}
                    placeholder={`Search here ${
                      mediaVal === "search" ? "" : mediaVal
                    } ...`}
                    value={curMedia === "*" ? "*" : curMedia}
                    onChange={(e) => {
                      handleSearch(e);
                    }}
                    size="small"
                    autoFocus={true}
                    className="border-2 border-gray-200 rounded text-gray-700 focus:outline-none focus:bg-white focus:border-purple-500"
                  />
                  <MdClose
                    size={35}
                    className="pt-4 cursor-pointer"
                    onClick={() => {
                      setMediaVal(disMedia ? "search" : "annotations");
                      setCurMedia("");
                      setDisMedia(disMedia ? true : false);
                      setIsShown(false);
                      setGroupToggle(false);
                      setMultiSearch(false);
                      setCustomQuestionModal(false);
                      handleFocus();
                    }}
                  />
                </div>
                <Space style={{ width: "100%" }} direction="vertical">
                  <Space wrap>
                    <Select
                      defaultValue={"all"}
                      className="py-2"
                      placeholder="Name / Description / Tag"
                      style={{ width: 180 }}
                      onChange={(e) => {
                        handleDropdown(e);
                      }}
                      options={[
                        { label: "All", value: "all" },
                        { label: "Name", value: "name" },
                        { label: "Description", value: "description" },
                        { label: "Tag", value: "tag" },
                      ]}
                    />
                  </Space>
                </Space>
                <Space style={{ width: "100%" }} direction="vertical">
                  <Space wrap>
                    <Select
                      defaultValue={auth ? "myGroup" : "publicGroup"}
                      style={{ width: 180 }}
                      className="py-2"
                      placeholder="Select Collection"
                      onChange={(e) => {
                        handleDropdown(e);
                      }}
                      options={[
                        {
                          label: "Public Collection",
                          value: "publicCollection",
                        },
                        { label: "My Collection", value: "myCollection" },
                        {
                          label: "Multiple Collection",
                          value: "multipleCollection",
                        },
                      ]}
                    />
                    {multiSearch && (
                      <Cascader
                        options={multiGroup}
                        defaultOpen={true}
                        placeholder="Select type"
                        className="py-2"
                        expandTrigger="hover"
                        displayRender={displayRender}
                        onChange={handleMulti}
                        multiple
                        maxTagCount="responsive"
                      />
                    )}
                  </Space>
                </Space>

                <Space style={{ width: "100%" }} direction="vertical">
                  <Cascader
                    options={[
                      {
                        value: "media",
                        label: "Media",
                      },
                      {
                        label: "Annotation",
                        value: "annotation",
                        children: [
                          {
                            value: "annotationName",
                            label: "Annotation Name",
                          },
                          {
                            value: "annotationTag",
                            label: "Annotation Tag",
                          },
                        ],
                      },
                    ]}
                    defaultValue={["media"]}
                    placeholder="Select type"
                    className="py-2"
                    expandTrigger="hover"
                    displayRender={displayRender}
                    onChange={handleSelectAnnotation}
                  />
                </Space>
                <IconButton
                  type="tertiary"
                  size="xs"
                  className="p-0 px-3 py-3"
                  onClick={() => {
                    toggle();
                    handleFocus();
                  }}
                >
                  <MdOutlineReportProblem size={20} />
                </IconButton>
              </div>

              {isShown && (
                <div className="col-span-3">
                  <a href="https://gitlab.com/servelots/papad/papad-api/-/issues/2">
                    <li>Put * to search any</li>
                    <li>
                      Custom Question will work only with users of POSTGREL DB
                      Users
                    </li>
                    <li>Follow the link for Search based development</li>
                  </a>
                </div>
              )}
            </div>
          </div>

          <div className="col-span-8 p-5">
            <Row gutter={[24, 16]}>
              {disMedia
                ? searchMedia.map((media, i) => (
                    <Col xs={{ span: 24 }} lg={{ span: 8 }} key={i}>
                      <CardLayout collection={media} />
                    </Col>
                  ))
                : searchAnno.map((anno, i) => (
                    <Col xs={{ span: 24 }} lg={{ span: 8 }} key={i}>
                      <AnnotationCard
                        annotationId={anno.uuid}
                        annoText={anno.annotation_text}
                        tags={anno.tags}
                        startTime={secToTimestamp(
                          anno.media_target.split("=").join().split(",")[1]
                        )}
                        endTime={secToTimestamp(
                          anno.media_target.split("=").join().split(",")[2]
                        )}
                        annoImage={anno.annotation_image}
                        createdDate={anno.created_at}
                        createdBy={anno.created_by}
                        updatedDate={anno.updated_at}
                        shareModal={shareModal}
                        deleteModal={deleteAnnoModal}
                        exportAnno={exportAnnotation}
                        loading={loading}
                      />
                    </Col>
                  ))}
            </Row>
          </div>

          {customQuestionModal && (
            <DialogModal
              open={customQuestionModal}
              onClose={() => setCustomQuestionModal(!customQuestionModal)}
              title={"Select custom question to be searched"}
            >
              <CustomQuestionModal
                onClose={() => setCustomQuestionModal(!customQuestionModal)}
                groupData={me}
                onSelectCQ={handleCustom}
              />
            </DialogModal>
          )}
          {deleteModal && (
            <DialogModal
              open={deleteModal}
              onClose={() => setDeleteModal(false)}
              title={`Are you sure you want to delete this annotation ?`}
            >
              <DeleteModal
                onClose={() => {
                  setDeleteModal(false);
                }}
                onClick={() => {
                  handleAnnotation(annoId);
                  setDeleteModal(false);
                }}
              />
            </DialogModal>
          )}
          {modelOpen && (
            <AntModel
              modelTitle={
                <div>
                  <div className="font-bold">Share your annotation</div>
                  <span>
                    {url[0] +
                      "/" +
                      url[2] +
                      "/" +
                      url[3] +
                      "/" +
                      url[4] +
                      "/" +
                      url[5] +
                      "/" +
                      url[6] +
                      `/annotation/${shareId}`}
                  </span>
                </div>
              }
              modelOpen={modelOpen}
              updateModel={updateModel}
              okText={"Share"}
              okType={"primary"}
            />
          )}
        </div>
      </div>
    </>
  );
};

export default Search;
