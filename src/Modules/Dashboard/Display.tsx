import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { secToTimestamp } from "../Media/Annotation";

export const Display = ({ data, id }: { data: any; id?: string }) => {
  const [grpId, setGrpId] = useState(null);
  const navigate = useNavigate();

  useEffect(() => {
    data?.results?.filter((x) => setGrpId(x.group.id));
  }, []);

  return (
    <ul className="w-full sm:space-y-0 sm:grid sm:grid-cols-2 lg:grid-cols-4 gap-2 p-2">
      {data.results &&
        data.results.map((y, key) => (
          <div key={y.uuid}>
            {id === "annotations" ? (
              // Annotation display
              <div className="rounded overflow-hidden shadow-lg mb-2 cursor-pointer">
                <div>
                  {y.annotation_image && (
                    <img
                      className="w-full mx-auto h-30 rounded-sm"
                      src={y.annotation_image}
                      alt={y.annotation_text}
                    />
                  )}
                  <div className="px-6 py-4">
                    <div className="font-bold text-xl mb-2">
                      {y.annotation_text}
                    </div>
                  </div>
                  <div className="grid grid-cols-4 p-1">
                    <div className="col-span-2">
                      {y.tags &&
                        y.tags.map((tag, index) => {
                          if (tag.name !== "") {
                            return (
                              <div className="inline-flex" key={index}>
                                <span className="bg-gray-200 rounded-full px-2 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">
                                  #{tag.name}
                                </span>
                              </div>
                            );
                          }
                          return false;
                        })}
                    </div>
                    <div className="flex gap-0.5 text-center">
                      {y.media_target &&
                        y.media_target
                          .split("=")
                          .splice(-1)
                          .join()
                          .split(",")
                          .map((fragment, key) => {
                            return (
                              <div key={key}>
                                <span className="w-full h-fit text-white inline-block bg-blue-600 rounded-md px-1 py-2 text-sm font-semibold mb-2 mr-2 float-left">
                                  {secToTimestamp(fragment)}
                                </span>
                              </div>
                            );
                          })}
                    </div>
                  </div>
                </div>
              </div>
            ) : (
              // Media display
              <div>
                <div
                  className="sm:space-y-0 sm:grid sm:grid-cols-1 lg:grid-cols-1"
                  onClick={() =>
                    navigate(`/collection/${y.group.id}/media/${y.uuid}`)
                  }
                  key={y.uuid}
                >
                  <div className="rounded-2xl overflow-hidden shadow-lg mb-2 max-w-auto cursor-pointer">
                    <div className="grid grid-cols-3 p-2 gap-5">
                      <div className="col-span-1">
                        <img
                          className="mx-auto w-fit"
                          src={process.env.PUBLIC_URL + "/images/papad.png"}
                          alt={y.name}
                        />
                      </div>
                      <div className="col-span-2 py-4">
                        <div className="col-span-2 font-bold text-2xl">
                          {y.name}
                        </div>
                        <div className="col-span-2 font-base text-base mb-2">
                          {y.description}
                        </div>
                      </div>
                    </div>
                    <div className="pl-3 w-full relative">
                      {y.tags &&
                        y.tags.map((tag, index) => {
                          if (tag.name !== "") {
                            return (
                              <div className="inline-flex" key={index}>
                                <span className="bg-gray-200 rounded-full px-2 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">
                                  #{tag.name}
                                </span>
                              </div>
                            );
                          }
                          return false;
                        })}
                    </div>
                  </div>
                </div>
              </div>
            )}
          </div>
        ))}
    </ul>
  );
};

export default Display;
