import React, { useEffect, useState } from "react";
import { MdGroups } from "react-icons/md";
import { BiImport } from "react-icons/bi";
import { BsPlusSquareDotted } from "react-icons/bs";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import { Button, DialogModal, ImportModal, Loader } from "../../Components";
import CreateCollection from "../Collection/CreateCollection";
import TagButton from "../../Components/Button/TagButton";
import { Card, Col, Row, Select, Space } from "antd";
import CardLayout from "../../Components/Cards/CardLayout";
import { Pagination } from "antd";
import type { PaginationProps } from "antd";

export interface IPage {
  next?: boolean;
  previous?: boolean;
}

const Dashboard = () => {
  const [recordings, setRecordings] = useState<any[]>([]);
  const [tags, setTags] = useState<any>([]);
  const [page, setPage] = useState({ previous: null, next: null });
  const [paginate, setPaginate] = useState<IPage>({
    previous: false,
    next: false,
  });
  const [privateGroups, setPrivateGroups] = useState<any[]>([]);
  const [loading, setLoading] = useState(true);
  const [createColModal, setCreateColModal] = useState(false);
  const navigate = useNavigate();
  const auth = localStorage.getItem("token");
  const baseURL = localStorage.getItem("prod_url");
  const [title, setTitle] = useState<any>(localStorage.getItem("dropDown"));
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [itemPage, setItemPage] = useState<number>(10);

  useEffect(() => {
    setTitle(localStorage.getItem("dropDown"));
    getRecordings();
    getAllTags();
    auth && getUser();
  }, []);

  useEffect(() => {
    setTitle(localStorage.getItem("dropDown"));
  }, [localStorage.getItem("dropDown")]);

  const getRecordings = () => {
    const options = {
      method: "GET",
      url: `${baseURL}api/v1/group/`,
      headers: {
        "Content-Type": "form-data",
      },
    };

    const AuthOptions = {
      method: "GET",
      url: `${baseURL}api/v1/group/`,
      headers: {
        "Content-Type": "form-data",
        Authorization: `Token ${auth}`,
      },
    };

    axios
      .request(auth ? AuthOptions : options)
      .then((response) => {
        setRecordings(response.data.results);
        setPage({ next: response.data.next, previous: response.data.previous });
        response.data.next === null && setPaginate({ next: true });
        response.data.previous === null && setPaginate({ previous: true });
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const getAllTags = () => {
    const options = {
      method: "GET",
      url: `${baseURL}api/v1/tags/?page_size=20`,
      headers: {
        "Content-Type": "multipart/form-data",
      },
    };

    axios
      .request(options)
      .then((response) => {
        setTags(response.data);
        setLoading(false);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const getPage = (id) => {
    const options = {
      method: "GET",
      url: id,
      headers: {
        "Content-Type": "multipart/form-data",
      },
    };

    axios
      .request(options)
      .then((response) => {
        setRecordings(response.data.results);
        setPage({ next: response.data.next, previous: response.data.previous });
        response.data.previous === null && setPaginate({ previous: true });
        response.data.next === null
          ? setPaginate({ next: true })
          : setPaginate({ next: false });
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const getUser = () => {
    const options = {
      method: "GET",
      url: `${baseURL}auth/users/me/`,
      headers: {
        Authorization: `Token ${auth}`,
      },
    };

    axios
      .request(options)
      .then((response) => {
        setPrivateGroups(response.data.groups);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const GetTags = () => {
    return (
      <div>
        {tags.map((tag, i) => {
          if (tag.name !== "" && tag.count !== 0) {
            return (
              <div
                className="inline-flex m-1 cursor-pointer"
                key={i}
                onClick={() => {
                  navigate(`/search/tag=${tag.name}`);
                }}
              >
                <TagButton
                  color="rgb(85, 172, 238)"
                  tagName={tag.name + " " + tag.count}
                />
              </div>
            );
          }
          return false;
        })}
      </div>
    );
  };

  const updateGroups = () => {
    getRecordings();
  };

  const handleDisplay = (id) => {
    if (id === "next") {
      page.next !== null && getPage(page.next);
    } else if (id === "prev") {
      page.previous === null
        ? setPaginate({ previous: true })
        : getPage(page.previous);
    }
  };

  const Paginations = () => {
    return (
      <div className="flex justify-center">
        {page.next === null && page.previous === null ? null : (
          <>
            <Button
              size="sm"
              type="tertiary"
              className="m-5 font-semibold border-2 w-fit rounded-md shadow-lg border-blue-600 -blue-600 disabled:opacity-25 focus:outline-none"
              onClick={(e) => {
                setTitle(localStorage.getItem("dropDown"));
                handleDisplay("prev");
              }}
              disabled={paginate.previous}
            >
              Previous
            </Button>
            <Button
              size="sm"
              type="tertiary"
              className="m-5 font-semibold border-2 w-min rounded-md shadow-lg border-blue-600 -blue-600 disabled:opacity-25 focus:outline-none"
              onClick={(e) => {
                setTitle(localStorage.getItem("dropDown"));
                handleDisplay("next");
              }}
              disabled={paginate.next}
            >
              Next
            </Button>
          </>
        )}
      </div>
    );
  };

  const handlePageChange = (page) => {
    setCurrentPage(page);
  };

  const startIndex = (currentPage - 1) * itemPage;
  const endIndex = startIndex + itemPage;
  const currentData = privateGroups.slice(startIndex, endIndex);

  const itemRender: PaginationProps["itemRender"] = (
    _,
    type,
    originalElement
  ) => {
    if (type === "prev") {
      return <a>Previous</a>;
    }
    if (type === "next") {
      return <a>Next</a>;
    }
    return originalElement;
  };

  const NoData = () => {
    return (
      <div className="flex gap-2 text-center justify-center font-semibold text-lg">
        <span>
          You can view media in public collections or private collections that
          you are a member of.
        </span>
      </div>
    );
  };
  const CollectionHead = () => {
    const [importModal, setImportModal] = useState(false);
    return (
      <div className="flex flex-col gap-10">
        <div className="font-bold text-3xl">Start with a Collection</div>
        <div className="flex flex-row w-full pb-10 gap-10">
          <Button
            size="sm"
            onClick={() => setCreateColModal(true)}
            className="flex flex-row lg:gap-3 md:gap-2 sm:gap-0 items-center px-2 py-6 mr-0 shadow-black shadow-2xl"
            type="primary"
          >
            <MdGroups size={25} fontStyle="bold" />
            Create a collection
          </Button>
          <Button
            size="sm"
            onClick={() => setImportModal(true)}
            className="flex flex-row  lg:gap-3 md:gap-2 sm:gap-0 items-center px-2 py-6 mr-0 shadow-black shadow-2xl"
            type="primary"
          >
            <BiImport size={25} fontStyle="bold" />
            Import a collection
          </Button>
          {importModal && (
            <DialogModal
              open={importModal}
              onClose={() => setImportModal(false)}
              title={"Import a Collection"}
            >
              <ImportModal
                type="collection"
                onClose={() => setImportModal(false)}
                updateGroups={updateGroups}
              />
            </DialogModal>
          )}
        </div>
      </div>
    );
  };

  const Cards = () => {
    useEffect(() => {
      auth || localStorage.getItem("dropDown")
        ? setTitle(localStorage.getItem("dropDown"))
        : setTitle("All Collections");
    }, []);

    const AntDropdown = () => {
      return (
        <>
          <Space direction="vertical">
            <Select
              defaultValue={localStorage.getItem("dropDown")}
              style={{ width: 200 }}
              className="w-min"
              onChange={(e) => {
                setTitle(e);
                localStorage.setItem("dropDown", e);
              }}
              options={[
                { label: "All Collections", value: "All Collections" },
                { label: "Public Collections", value: "Public Collections" },
                auth && { label: "My Collections", value: "My Collections" },
              ]}
            />
          </Space>
        </>
      );
    };

    const cardData = {
      title: title,
      dropDown: <AntDropdown />,
    };
    const CollectionsCard = () => {
      return (
        <>
          {auth ? (
            <Row gutter={[24, 16]}>
              {localStorage.getItem("dropDown") === "My Collections" && (
                <>
                  {privateGroups?.length === 0 && (
                    <Col span={24}>
                      <CardLayout noDataText={<NoData />} />
                    </Col>
                  )}
                  <Col
                    xs={{ span: 24 }}
                    lg={{ span: 8 }}
                    className="cursor-pointer"
                  >
                    <Card
                      loading={loading}
                      style={{ borderColor: "rgb(179, 222, 236)" }}
                      onClick={() =>
                        auth ? setCreateColModal(true) : navigate("/auth/login")
                      }
                    >
                      <div className="grid place-items-center justify-center h-[13.2rem]">
                        <BsPlusSquareDotted
                          size={60}
                          style={{ textAlign: "center" }}
                        />
                        <span>Create a Collection</span>
                      </div>
                    </Card>
                  </Col>
                  {privateGroups?.map((grp, i) => (
                    <Col xs={{ span: 24 }} lg={{ span: 8 }} key={i}>
                      <CardLayout collection={grp} />
                    </Col>
                  ))}
                </>
              )}
              {localStorage.getItem("dropDown") === "All Collections" && (
                <>
                  <Col>
                    <>
                      <Row gutter={[24, 16]}>
                        <Col span={24}>
                          <div className="text-[16px] font-semibold mb-3">
                            My Collection
                          </div>
                          {privateGroups?.length === 0 && (
                            <CardLayout noDataText={<NoData />} />
                          )}
                        </Col>
                      </Row>
                      <Row gutter={[24, 16]}>
                        <Col
                          xs={{ span: 24 }}
                          lg={{ span: 8 }}
                          className="cursor-pointer"
                        >
                          <Card
                            loading={loading}
                            style={{ borderColor: "rgb(179, 222, 236)" }}
                            onClick={() =>
                              auth
                                ? setCreateColModal(true)
                                : navigate("/auth/login")
                            }
                          >
                            <div className="grid place-items-center justify-center h-[13.2rem]">
                              <BsPlusSquareDotted
                                size={60}
                                style={{ textAlign: "center" }}
                              />
                              <span>Create a Collection</span>
                            </div>
                          </Card>
                        </Col>
                        {currentData?.map((grp, i) => (
                          <Col xs={{ span: 24 }} lg={{ span: 8 }} key={i}>
                            <CardLayout collection={grp} key={i} />
                          </Col>
                        ))}
                      </Row>
                      <Row gutter={[24, 24]}>
                        <Col span={24}>
                          <div className="flex justify-center mt-5">
                            <Pagination
                              current={currentPage}
                              pageSize={itemPage}
                              total={privateGroups.length}
                              itemRender={itemRender}
                              onChange={handlePageChange}
                              showQuickJumper
                              showTotal={(total) => `Total ${total} items`}
                            />
                          </div>
                        </Col>
                      </Row>
                      <hr className="mt-6 border-b-1 border-gray-200 pt-5" />
                      <Row gutter={[24, 24]}>
                        <Col span={24}>
                          <div className="text-[16px] font-semibold mb-3">
                            Public Collection
                          </div>
                          {recordings?.length === 0 && (
                            <CardLayout noDataText={<NoData />} />
                          )}
                        </Col>
                        {recordings?.map((grp, i) => (
                          <Col xs={{ span: 24 }} lg={{ span: 8 }} key={i}>
                            <CardLayout collection={grp} />
                          </Col>
                        ))}
                      </Row>

                      {recordings?.length !== 0 && <Paginations />}
                    </>
                  </Col>
                </>
              )}
              {localStorage.getItem("dropDown") === "Public Collections" && (
                <>
                  <Col span={24}>
                    <Row gutter={[24, 16]}>
                      {recordings?.map((grp, i) => (
                        <Col xs={{ span: 24 }} lg={{ span: 8 }} key={i}>
                          <CardLayout collection={grp} />
                        </Col>
                      ))}
                    </Row>
                    <Row>{recordings?.length !== 0 && <Paginations />}</Row>
                  </Col>
                </>
              )}

              {createColModal && (
                <DialogModal
                  open={createColModal}
                  onClose={() => setCreateColModal(false)}
                  title={"Create a Collection"}
                >
                  <CreateCollection
                    onClose={() => setCreateColModal(false)}
                    path="add"
                  />
                </DialogModal>
              )}
            </Row>
          ) : (
            <>
              {recordings?.length === 0 && (
                <Col span={24}>
                  <CardLayout noDataText={<NoData />} />
                </Col>
              )}
              <Col>
                <Row gutter={[24, 24]}>
                  {recordings?.map((grp, i) => (
                    <Col xs={{ span: 24 }} lg={{ span: 8 }} key={i}>
                      <CardLayout collection={grp} />
                    </Col>
                  ))}
                </Row>
                {recordings?.length !== 0 && <Paginations />}
              </Col>
            </>
          )}
        </>
      );
    };
    return (
      <>
        <div className="mx-4">
          <Row>
            <Col span={24}>
              {auth ? (
                <Card
                  title={cardData?.title}
                  extra={cardData?.dropDown}
                  loading={loading}
                >
                  <CollectionsCard />
                  {title === "My Collection"}
                  {title === "Public Collection" && recordings.length !== 0 && (
                    <Paginations />
                  )}
                </Card>
              ) : (
                <Card loading={loading}>
                  <CollectionsCard />
                  {title === "My Collection"}
                  {title === "Public Collection" && recordings.length !== 0 && (
                    <Paginations />
                  )}
                </Card>
              )}
            </Col>
          </Row>
          <div className="mt-4">
            <Row>
              <Col>
                <Card loading={loading}>{tags && <GetTags />}</Card>
              </Col>
            </Row>
          </div>
        </div>
      </>
    );
  };

  if (loading) {
    return <Loader />;
  }

  return (
    <div className="relative container w-full p-2 gap-2 max-h-fit">
      <Cards />
    </div>
  );
};

export default Dashboard;
