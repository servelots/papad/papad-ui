import axios from "axios";
import React, { useCallback, useEffect, useState } from "react";
import { MdAdd } from "react-icons/md";
import { useNavigate } from "react-router-dom";
import { IconButton, Loader } from "../../Components";
import { Input, Select, Space, Button } from "antd";
import AntButton from "antd/es/button";

const EditUploadForm = ({
  onClose,
  mediaId,
  notify,
  mediaName,
  mediaDescription,
  mediaTags,
  groupID,
  onSuccess,
}: {
  onClose?;
  mediaId?: string;
  notify?: Function;
  mediaName: string;
  mediaDescription: string;
  mediaTags: string[];
  groupID: string;
  onSuccess?: () => void;
}) => {
  const [name, setName] = useState(mediaName);
  const [description, setDescription] = useState(mediaDescription);
  const [allTags, setAllTags] = useState<any[]>(mediaTags);
  const [error, setError] = useState<string>("");
  const [disabled, setDisabled] = useState(false);
  const [loading, setLoading] = useState(false);
  const maxWords = 250;
  const [{ content, wordCount }, setContent] = useState({
    content: description,
    wordCount: 0,
  });
  const [newTag, setNewTag] = useState("");
  const [selectedItems, setSelectedItems] = useState<string[]>([]);
  const [TAGS, setTAGS] = useState<string[]>([]);

  const navigate = useNavigate();
  const auth = localStorage.getItem("token");
  const baseURL = localStorage.getItem("prod_url");
  const { Option } = Select;

  useEffect(() => {
    description !== "" && setFormattedContent(description);
  }, []);

  useEffect(() => {
    if (allTags.length !== 0) {
      getUsersInArray();
      setSelectedItems(TAGS);
    }
  }, [allTags]);

  const getMedia = () => {
    const options = {
      method: "GET",
      url: `${baseURL}api/v1/archive/${mediaId}/`,
    };

    axios
      .request(options)
      .then((response) => {
        setName(response.data.name);
        setDescription(response.data.description);
        setFormattedContent(response.data.description);
        setAllTags(response.data.tags);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const postRecord = (e) => {
    e.preventDefault();

    const formData = new FormData();
    formData.append("name", name);
    formData.append("description", description);

    const options = {
      method: "PUT",
      url: `${baseURL}api/v1/archive/${mediaId}/`,
      headers: {
        "Content-Type": "application/json",
        Authorization: `Token ${auth}`,
      },
      data: formData,
    };

    axios
      .request(options)
      .then((response) => {
        console.log(response);
        notify("sucess");
        navigate(`/collection/${groupID}/media/${response.data.uuid}`);
        onClose();
        onSuccess();
      })
      .catch((error) => {
        console.error(error);
        notify("error");
        setError(error.response.data.detail);
        setDisabled(false);
      });
  };

  const handleTag = (newtag) => {
    const addOptions = {
      method: "PUT",
      url: `${baseURL}api/v1/archive/add_tag/${mediaId}}/`,
      headers: {
        "Content-Type": "application/json",
        Authorization: `Token ${auth}`,
      },
      data: { tags: [newTag] },
    };

    axios
      .request(addOptions)
      .then((response) => {
        console.log(response.data);
        setNewTag("");
        setSelectedItems([]);
        setTAGS([]);
        getMedia();
        setDisabled(false);
      })
      .catch((error) => {
        console.error(error);
        setDisabled(false);
      });
  };

  const handleDelTag = (tag) => {
    const deleteOptions = {
      method: "PUT",
      url: `${baseURL}api/v1/archive/remove_tag/${mediaId}}/`,
      headers: {
        "Content-Type": "application/json",
        Authorization: `Token ${auth}`,
      },
      data: { tags: [tag] },
    };

    axios
      .request(deleteOptions)
      .then((response) => {
        console.log(response.data);
        setNewTag("");
        setSelectedItems([]);
        setTAGS([]);
        getMedia();
        setDisabled(false);
        setLoading(false);
      })
      .catch((error) => {
        console.error(error);
        setLoading(false);
      });
  };

  const setFormattedContent = useCallback(
    (text) => {
      let words = text.split(" ").filter(Boolean);
      if (words.length > maxWords) {
        setContent({
          content: words.slice(0, maxWords).join(" "),
          wordCount: maxWords,
        });
      } else {
        setContent({ content: text, wordCount: words.length });
      }
    },
    [maxWords, setContent]
  );

  const getUsersInArray = () => {
    allTags.map((item) => item.name !== "" && TAGS.push(item.name));
  };

  const onNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setNewTag(event.target.value);
  };

  const handle = (value: string) => {
    allTags
      .filter((tag) => tag.name === value)
      .map((tag) => handleDelTag(tag.id));
  };

  if (loading) {
    return <Loader />;
  }

  return (
    <div>
      <div className="item-align-center min-w-full max-w-screen">
        <div className="flex justify-center">
          <div className="m-4">
            <form onSubmit={(e) => postRecord(e)}>
              <div className="md:flex md:items-center m-5 mb-6">
                <div className="md:w-1/3">
                  <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-0 pr-4">
                    Name
                  </label>
                </div>
                <div className="md:w-2/3">
                  <input
                    required
                    className="bg-gray-200 border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 focus:outline-none focus:bg-white focus:border-purple-500"
                    type="text"
                    maxLength={300}
                    value={name}
                    onChange={(e) => {
                      setName(e.target.value);
                    }}
                  />
                </div>
              </div>

              <div className="md:flex md:items-center m-5 mb-6">
                <div className="md:w-1/3">
                  <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-0 pr-4">
                    Desciption
                  </label>
                </div>
                <div className="md:w-2/3 relative">
                  <textarea
                    className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                    rows={5}
                    value={content}
                    onChange={(e) => {
                      setFormattedContent(e.target.value);
                      setDescription(e.target.value);
                    }}
                    required
                  />
                  <span className="font-normal text-xs text-blueGray-400 absolute bottom-0 right-0 p-2">
                    {wordCount}/{maxWords}
                  </span>
                </div>
              </div>
              <div className="md:flex md:items-center m-5 mb-6">
                <div className="md:w-1/3">
                  <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-0 pr-4">
                    Tags
                  </label>
                </div>
                <div className="flex flex-col gap-5">
                  <Select
                    mode="multiple"
                    style={{ width: "100%" }}
                    value={selectedItems}
                    onChange={setSelectedItems}
                    autoClearSearchValue={false}
                    open={false}
                    onDeselect={handle}
                  >
                    {allTags?.map(
                      (item, key) =>
                        item !== "" && (
                          <Option value={item.id} label={item.name} key={key}>
                            <Space>{item.name}</Space>
                          </Option>
                        )
                    )}
                  </Select>
                  <Space direction="vertical" size="middle">
                    <Space.Compact style={{ width: "100%" }}>
                      <Input
                        placeholder="Add new tag"
                        allowClear
                        onPressEnter={() => handleTag(newTag)}
                        onChange={onNameChange}
                        size="large"
                        value={newTag}
                      />
                      <Button
                        icon={MdAdd}
                        className="flex"
                        type="primary"
                        onClick={() => handleTag(newTag)}
                      >
                        Add Tag
                      </Button>
                    </Space.Compact>
                  </Space>
                </div>
              </div>

              {error !== "" && (
                <div className="flex items-center justify-center p-5 border-b border-solid border-slate-200 rounded-t">
                  <h5 className="text-xl font-medium text-red-600">{error}</h5>
                </div>
              )}
              <div className="text-center mt-6 p-5">
                <AntButton
                  className="bg-[#108ee9]"
                  type="primary"
                  size="middle"
                  // htmlType="submit"
                  disabled={disabled || loading || name === ""}
                  onClick={(e) => {
                    setDisabled(true);
                    postRecord(e);
                  }}
                >
                  Submit
                </AntButton>
                <AntButton type="text" size="middle" onClick={onClose}>
                  Cancel
                </AntButton>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default EditUploadForm;
