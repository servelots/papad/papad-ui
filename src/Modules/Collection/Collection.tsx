import React, { Fragment, useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import axios from "axios";
import {
  MdDataSaverOn,
  MdModeEdit,
  MdOutlineArrowBack,
  MdOutlineDelete,
  MdPersonAdd,
  MdSettings,
  MdCheckCircle,
  MdOutlinePermMedia,
  MdHome,
} from "react-icons/md";
import {
  Button,
  DeleteModal,
  CopyModal,
  ImportModal,
  Loader,
  CustomModal,
  DialogModal,
  Line,
  Graph,
  CircularLoader,
} from "../../Components";
import { BiExport, BiImport, BiQuestionMark } from "react-icons/bi";
import { IoAddOutline } from "react-icons/io5";
import { ImStatsDots } from "react-icons/im";
import UploadForm from "./UploadForm";
import CreateCollection from "./CreateCollection";
import { Menu, Transition } from "@headlessui/react";
import {
  notification,
  Space,
  Badge,
  Card,
  Popover,
  Row,
  Col,
  Tooltip,
  Typography,
} from "antd";
import TagButton from "../../Components/Button/TagButton";
import AntButton from "antd/lib/button";
import CardLayout from "../../Components/Cards/CardLayout";
import AddUsersModal from "../../Components/Modal/AddUsersModal";
import EditUploadForm from "./EditUploadForm";
import AntDropDown from "../../Components/Dropdown/AntDropdown";
import AntModel from "../../Components/Modal/AntModel";
import { BsPlusSquareDotted } from "react-icons/bs";
import { RxQuestionMark } from "react-icons/rx";
import WordCloud from "../../Components/Stats/WordCloud";
import Paragraphs from "../../Components/Paragraph/Paragraphs";
const { Paragraph, Text } = Typography;

interface IPage {
  next?: boolean;
  previous?: boolean;
}

type NotificationType = "success" | "info" | "warning" | "error";

const Collection = ({
  groupId,
  groupName,
  groupDesc,
}: {
  groupId?: string;
  groupName?: string;
  groupDesc?: string;
}) => {
  const [recordings, setRecordings] = useState<any[]>([]);
  const [groupData, setGroupData] = useState<any>([]);
  const [page, setPage] = useState({ previous: "", next: "" });
  const [paginate, setPaginate] = useState<IPage>({
    previous: false,
    next: false,
  });
  const [count, setCount] = useState(0);
  const [delUser, setDelUser] = useState("");
  const [deleteUserName, setDeleteUserName] = useState("");
  const [me, setMe] = useState<any>([]);
  const [grpUsers, setGrpUsers] = useState<any[]>([]);
  const [grpCustom, setGrpCustom] = useState<any[]>([]);
  const [error, setError] = useState<string>("");
  const [copyModal, setCopyModal] = useState(false);
  const [mediaId, setMediaId] = useState("");
  const [mediaValue, setMediaValue] = useState("");
  const [copyExtra, setCopyExtra] = useState<any[]>([]);
  const [loading, setLoading] = useState(true);
  const [openRadio, setOpenRadio] = useState(1);
  const [importModal, setImportModal] = useState(false);
  const [deleteCustomModal, setDeleteCustomModal] = useState(false);
  const [deleteCustomQuestion, setDeleteCustomQuestion] = useState(false);
  const [deleteMediaModal, setDeleteMediaModal] = useState(false);
  const [deleteUserModal, setDeleteUserModal] = useState(false);
  const [superUser, setSuperUser] = useState<any>(null);
  const [mediaStatistics, setMediaStatistics] = useState<any>([]);
  const [annoStatistics, setAnnoStatistics] = useState<any>([]);
  const [uploadModal, setUploadModal] = useState(false);
  const [editModal, setEditModal] = useState(false);
  const [groupKnowledge, setGroupKnowledge] = useState<any>([]);
  const [tag, setTag] = useState<any>([]);
  const [editColModal, setEditColModal] = useState(false);
  const [addUserColModal, setAddUserColModal] = useState(false);
  const [customColModal, setCustomColModal] = useState(false);
  const [keepColModal, setKeepColModal] = useState(false);
  const [relations, setRelations] = useState(false);
  const [api, contextHolder] = notification.useNotification();
  const [modelOpen, setModelOpen] = useState(false);
  const [mediaTags, setMediaTags] = useState<string[]>([]);
  const [mediaDescription, setMediaDescription] = useState<string>("");

  const navigate = useNavigate();
  const { id } = useParams();
  const auth = localStorage.getItem("token");
  const baseURL = localStorage.getItem("prod_url");
  const userUUID = localStorage.getItem("userUUID");
  const isMember = localStorage.getItem("isMember");

  const openNotificationWithIcon = (type: NotificationType, msg) => {
    api[type]({
      message: msg,
      placement: "topRight",
    });
  };

  useEffect(() => {
    window.location.hash === "#relations" &&
      (window.location.hash = "#relations");
    if (auth) {
      getUser();
    }
    getGroup();
    getRecordings();
  }, []);

  useEffect(() => {
    if (openRadio === 1) {
      getGroupKnowledgeGraph();
    }
    if (openRadio === 3) {
      getGroupKnowledgeGraph();
    }
    if (openRadio === 2) {
      getMediaStatistics();
      getAnnoStatistics();
    }
  }, [openRadio]);

  useEffect(() => {
    const currentHash = window.location.hash;
    if (currentHash === "#relations") {
      window.location.hash = "";
      setRelations(false);
    } else {
      if (relations === true) {
        window.location.hash = "relations";
      }
    }
  }, [relations]);

  const getRecordings = () => {
    const options = {
      method: "GET",
      url: `${baseURL}api/v1/archive/?group=${id}`,
      headers: { "Content-Type": "application/json" },
    };

    axios
      .request(options)
      .then((response) => {
        setRecordings(response.data.results);
        setCount(response.data.count);
        setPage({ next: response.data.next, previous: response.data.previous });
        response.data.next === null && setPaginate({ next: true });
        response.data.previous === null && setPaginate({ previous: true });
        setLoading(false);
      })
      .catch((e) => {
        console.log(e);
        setError(e?.message);
      });
  };

  const getGroup = () => {
    const options = {
      method: "GET",
      url: `${baseURL}api/v1/group/${id}/`,
      headers: {
        "Content-Type": "application/json",
      },
    };

    const authOptions = {
      method: "GET",
      url: `${baseURL}api/v1/group/${id}/`,
      headers: {
        "Content-Type": "application/json",
        Authorization: `Token ${auth}`,
      },
    };

    axios
      .request(auth ? authOptions : options)
      .then((response) => {
        setGroupData(response.data);
        setGrpUsers(response.data.users);
        setGrpCustom(response.data.extra_group_questions);
        setTag(response.data.tags);
        userPartOfCollection(response.data.users);
        if (!auth && !response.data.is_public) {
          window.location.href = "/auth/login";
        }
      })
      .catch((e) => {
        console.log(e);
        setError(e.message);
      });
  };

  const getPage = (id) => {
    const options = {
      method: "GET",
      url: id,
      headers: {
        "Content-Type": "multipart/form-data",
      },
    };

    axios
      .request(options)
      .then((response) => {
        setRecordings(response.data.results);
        setPage({ next: response.data.next, previous: response.data.previous });
        response.data.next === null
          ? setPaginate({ next: true })
          : setPaginate({ next: false });
        response.data.previous === null && setPaginate({ previous: true });
        setLoading(false);
      })
      .catch((e) => {
        console.log(e);
        setError(e.message);
      });
  };

  const getUser = () => {
    const options = {
      method: "GET",
      url: `${baseURL}auth/users/me/`,
      headers: {
        Authorization: `Token ${auth}`,
      },
    };

    axios
      .request(options)
      .then((response) => {
        setMe(response.data);
        setSuperUser(response.data.is_superuser);
      })
      .catch((e) => {
        console.log(e);
        setError(e.message);
      });
  };

  const getMediaStatistics = () => {
    const options = {
      method: "GET",
      url: `${baseURL}api/v1/groupmediastats/${id}/`,
      headers: {
        Authorization: auth && `Token ${auth}`,
      },
    };

    axios
      .request(options)
      .then((response) => {
        setMediaStatistics(response.data);
      })
      .catch((error) => {
        console.error(error.message);
      });
  };

  const getAnnoStatistics = () => {
    const options = {
      method: "GET",
      url: `${baseURL}api/v1/groupannotationstats/${id}/`,
      headers: {
        Authorization: auth && `Token ${auth}`,
      },
    };

    axios
      .request(options)
      .then((response) => {
        setAnnoStatistics(response.data);
      })
      .catch((error) => {
        console.error(error.message);
      });
  };

  const getGroupKnowledgeGraph = () => {
    const options = {
      method: "GET",
      url: `${baseURL}api/v1/group/taggraph/${id}/`,
    };

    const authOptions = {
      method: "GET",
      url: `${baseURL}api/v1/group/taggraph/${id}/`,
      headers: {
        Authorization: `Token ${auth}`,
      },
    };

    axios
      .request(auth ? authOptions : options)
      .then((response) => {
        setGroupKnowledge(response.data);
      })
      .catch((error) => {
        console.error(error.message);
      });
  };

  const handleDisplay = (id) => {
    if (id === "next") {
      page.next !== null && getPage(page.next);
    } else if (id === "prev") {
      page.previous === null
        ? setPaginate({ previous: true })
        : getPage(page.previous);
    }
  };

  const userPartOfCollection = (users) => {
    const isMember = users.filter((user) => user.id === userUUID).length > 0;
    localStorage.setItem("isMember", isMember ? "true" : "false");
  };

  const Pagination = () => {
    return (
      <>
        {page.next === null && page.previous === null ? null : (
          <div className="flex justify-center">
            <Button
              size="sm"
              type="tertiary"
              className="m-5 font-semibold border-2 w-fit rounded-lg shadow-lg border-blue-600 -blue-600 disabled:opacity-25 focus:outline-none"
              onClick={(e) => {
                handleDisplay("prev");
                setLoading(true);
              }}
              disabled={paginate.previous}
            >
              Previous
            </Button>
            <Button
              size="sm"
              type="tertiary"
              className="p-2 m-5 font-semibold border-2 w-min rounded-lg shadow-lg border-blue-600 -blue-600 disabled:opacity-25 focus:outline-none"
              onClick={(e) => {
                handleDisplay("next");
                setLoading(true);
              }}
              disabled={paginate.next}
            >
              Next
            </Button>
          </div>
        )}
      </>
    );
  };

  const handleDeleteUser = (user) => {
    const form = new FormData();
    form.append("user", user);

    const options = {
      method: "PUT",
      url: `${baseURL}api/v1/group/remove_user/${id}/`,
      headers: {
        "Content-Type": "multipart/form-data",
        Authorization: `Token ${auth}`,
      },
      data: form,
    };

    axios
      .request(options)
      .then((response) => {
        console.log(response);
        setDeleteUserModal(false);
        getGroup();
        setLoading(false);
      })
      .catch((e) => {
        console.log(e);
        setError(e.response.data.detail);
        setLoading(false);
      });
  };

  const handleMedia = (mediaId) => {
    const options = {
      method: "DELETE",
      url: `${baseURL}api/v1/archive/${mediaId}/`,
      headers: {
        Authorization: `Token ${auth}`,
      },
    };

    axios
      .request(options)
      .then((response) => {
        console.log(response);
        getGroup();
        getRecordings();
        setDeleteMediaModal(false);
        setLoading(false);
      })
      .catch((e) => {
        console.log(e);
        setError(e.response.data.detail);
        setLoading(false);
      });
  };

  const handleExport = () => {
    const options = {
      method: "POST",
      url: `${baseURL}api/v1/export/`,
      headers: {
        Authorization: `Token ${auth}`,
        "Content-Type": "application/json",
      },
      data: { type: "group", id: id },
    };

    axios
      .request(options)
      .then((response) => {
        console.log(response.data);
        navigate("/requests");
      })
      .catch((error) => {
        console.error(error);
        setError(error.response.data);
      });
  };

  const notifyToast = (id) => {
    switch (id) {
      case "success":
        openNotificationWithIcon("success", "Successfully Uploaded Media");
        break;
      case "error":
        openNotificationWithIcon("error", "Failed to Uploaded Media");
        break;
    }
  };

  const GroupInfo = () => {
    const UserDelete = ({ userData }) => {
      const [clicked, setClicked] = useState(false);
      const [hovered, setHovered] = useState(false);

      const hide = () => {
        setClicked(false);
        setHovered(false);
      };

      const handleHoverChange = (open: boolean) => {
        setHovered(open);
        setClicked(false);
      };

      const handleClickChange = (open: boolean) => {
        setHovered(false);
        setClicked(open);
      };

      const showContent = () => {
        return (
          <div className="flex flex-col gap-2 justify-between place-content-center items-center">
            <div>
              {userData.first_name} {userData.last_name}
            </div>
            <div>@{userData.username}</div>
            <div>{userData.id}</div>

            {/* <div
              className="border-2 border-red-500 p-1 rounded-full align-middle items-center text-center self-center"
              onClick={() => {
                setDelUser(userData.id);
                setDeleteUserModal(true);
                setDeleteUserName(userData.first_name);
              }}
            >
              <MdOutlineDelete size={15} />
            </div> */}
          </div>
        );
      };

      return (
        <Space size={[0, 8]} wrap>
          <Popover
            content={showContent}
            trigger="hover"
            open={hovered}
            onOpenChange={handleHoverChange}
          >
            <Popover
              content={showContent}
              trigger="click"
              open={clicked}
              onOpenChange={handleClickChange}
            >
              <AntButton
                type="text"
                shape="round"
                size="middle"
                className="border-green-500 border-2 font-semibold"
              >
                {userData.first_name}
              </AntButton>
            </Popover>
          </Popover>
        </Space>
      );
    };

    const CollectionSettings = () => (
      <div className="flex gap-2">
        <Badge
          count={`${groupData?.is_public ? "Public" : "Private"}`}
          showZero
          color={`${groupData?.is_public ? "#52c41a" : "#faad14"}`}
          className="ml-4 mt-[-2]"
        />
        <div className="flex gap-2">
          {relations ? (
            <AntButton
              type="text"
              className="px-2 mr-[3rem] rounded-lg border-2 border-sky-600 cursor-pointer text-sm font-semibold"
              onClick={() => setRelations(!relations)}
            >
              <div className="flex gap-2  items-center">
                <MdOutlinePermMedia className="h-[1.2rem] w-[1.2rem]" />
                {"Media"}
              </div>
            </AntButton>
          ) : (
            <AntButton
              type="text"
              className="px-2 mr-[3rem] rounded-lg border-2 border-sky-600 cursor-pointer text-sm font-semibold"
              onClick={() => {
                setRelations(!relations);
              }}
            >
              <div className="flex gap-2 items-center">
                <ImStatsDots className="h-[1rem] w-[1rem]" />
                {"Relations"}
              </div>
            </AntButton>
          )}
        </div>
        {isMember === "true" ? (
          <div className="flex gap-2 absolute top-2 right-2">
            <Menu as="div" className="flex">
              <div className="items-center flex rounded-xl hover:bg-[#d6ebfd] p-2 focus:outline-none ">
                <Menu.Button className="flex text-sm focus:outline-none focus:ring-none focus:ring-none">
                  <MdSettings className="h-[1.5rem] w-[1.5rem]" size={50} />
                </Menu.Button>
              </div>
              <Transition
                as={Fragment}
                enter="transition ease-out duration-100"
                enterFrom="transform opacity-0 scale-95"
                enterTo="transform opacity-100 scale-100"
                leave="transition ease-in duration-75"
                leaveFrom="transform opacity-100 scale-100"
                leaveTo="transform opacity-0 scale-95"
              >
                <div className="text-md font-semibold absolute right-0 px-2 z-10 mt-[3.6rem] min-w-[16rem] max-w-[100%] origin-top-right rounded-md text-black bg-white py-1 shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none ">
                  <Menu.Items className="divide-y divide-[#e5e7eb] w-full">
                    <Menu.Item>
                      <div className="my-2">
                        <div className="flex flex-row w-full my-2 text-sm font-normal">
                          <div className="flex flex-col w-full">
                            <div
                              className="p-2 hover:bg-[#acd3ee] cursor-pointer flex  justify-between"
                              onClick={() => setEditColModal(true)}
                            >
                              <span className="flex place-items-center">
                                <MdModeEdit className="pr-2" size={25} />
                                <span> Edit Collection </span>
                              </span>
                            </div>

                            <div
                              className="p-2 hover:bg-[#acd3ee] cursor-pointer flex  justify-between"
                              onClick={() => setAddUserColModal(true)}
                            >
                              <span className="flex place-items-center">
                                <MdPersonAdd className="pr-2" size={25} />
                                <span> Add User</span>
                              </span>
                            </div>

                            <div
                              className="p-2 hover:bg-[#acd3ee] cursor-pointer flex  justify-between"
                              onClick={() => setCustomColModal(true)}
                            >
                              <span className="flex place-items-center">
                                <IoAddOutline className="pr-2" size={25} />
                                <span>Add Custom Question</span>
                              </span>
                            </div>
                            {grpCustom.length !== 0 && (
                              <>
                                <div
                                  className="p-2 hover:bg-[#acd3ee] cursor-pointer flex  justify-between"
                                  onClick={() => {
                                    setDeleteCustomModal(true);
                                    setDeleteCustomQuestion(false);
                                  }}
                                >
                                  <span className="flex place-items-center">
                                    <MdModeEdit className="pr-2" size={25} />
                                    <span>Edit Custom Question</span>
                                  </span>
                                </div>
                                <div
                                  className="p-2 hover:bg-[#acd3ee] cursor-pointer flex  justify-between"
                                  onClick={() => {
                                    setDeleteCustomModal(true);
                                    setDeleteCustomQuestion(true);
                                  }}
                                >
                                  <span className="flex place-items-center">
                                    <MdOutlineDelete
                                      className="pr-2"
                                      size={25}
                                    />
                                    <span>Delete Custom Question</span>
                                  </span>
                                </div>
                              </>
                            )}

                            <div
                              className="p-2 hover:bg-[#acd3ee] cursor-pointer flex  justify-between"
                              onClick={() => setKeepColModal(true)}
                            >
                              <span className="flex place-items-center">
                                <MdDataSaverOn className="pr-2" size={25} />
                                <span>Keep deleted data</span>
                              </span>
                            </div>
                            <div
                              className="p-2 hover:bg-[#acd3ee] cursor-pointer flex  justify-between"
                              onClick={() => handleExport()}
                            >
                              <span className="flex place-items-center">
                                <BiExport className="pr-2" size={25} />
                                <span> Export Collection</span>
                              </span>
                            </div>
                            <div
                              className="p-2 hover:bg-[#acd3ee] cursor-pointer flex  justify-between"
                              onClick={() => setImportModal(true)}
                            >
                              <span className="flex place-items-center">
                                <BiImport className="pr-2" size={25} />
                                <span>Import a Media</span>
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </Menu.Item>
                  </Menu.Items>
                </div>
              </Transition>
            </Menu>
          </div>
        ) : (
          <div className="flex gap-2 absolute top-2 right-2">
            <div className="items-center flex rounded-xl hover:bg-[#d6ebfd] p-2 focus:outline-none ">
              <Tooltip
                title={
                  "You cannot upload to this collection. You need to be an editor of the collection to upload or annotate."
                }
              >
                <RxQuestionMark className="h-[1.5rem] w-[1.5rem]" size={50} />
              </Tooltip>
            </div>
          </div>
        )}
      </div>
    );

    return (
      <>
        <div className="mt-3">
          <div className="flex space-x-1 pb-2">
            <div className="self-center cursor-pointer">
              <a
                href={`/`}
                className="text-gray-500 text-md flex gap-1 px-1 hover:bg-gray-300 hover:text-black rounded"
              >
                <MdHome
                  size={16}
                  className="mt-[0.2rem]"
                  style={{ color: "#1677ff" }}
                />
                <Tooltip title={"Back to Home"}>
                  <Text style={{ width: "100%", color: "#1677ff" }}>
                    {"Home"}
                  </Text>
                </Tooltip>
              </a>
            </div>

            <div className="text-gray-500">{"/"}</div>
            <div className="text-gray-500 rounded -mt-[0.1rem]">
              <Tooltip title={groupData?.name}>
                <Text
                  ellipsis={{ tooltip: `${groupData?.name}` }}
                  style={{ width: "100%", maxWidth: 150 }}
                >
                  {groupData?.name}
                </Text>
              </Tooltip>
            </div>
          </div>
          <Space direction="vertical" size="middle" style={{ width: "100%" }}>
            <Card
              loading={loading}
              title={
                <>
                  <div className="flex justify-between h-[3rem] mt-4">
                    <div className="flex gap-2 w-[80%]">
                      <div className="justify-between items-center sm:block hidden">
                        <span className="text-lg font-semibold">
                          {groupData?.name && (
                            <Tooltip title={groupData?.name}>
                              <Paragraph>{groupData?.name}</Paragraph>
                            </Tooltip>
                          )}
                        </span>
                      </div>
                    </div>
                  </div>
                </>
              }
              extra={<CollectionSettings />}
            >
              <div className="flex justify-between self-center items-center sm:hidden">
                <span className="text-lg font-semibold">{groupData?.name}</span>
                <Badge
                  count={`${groupData?.is_public ? "Public" : "Private"}`}
                  showZero
                  color={`${groupData?.is_public ? "#52c41a" : "#faad14"}`}
                  className="ml-4"
                />
              </div>
              <div className="mt-2 text-sm h-auto min-h-24 max-h-40 line-clamp-5 hover:line-clamp-none ">
                {groupData?.description && (
                  <Paragraphs
                    isExpand={true}
                    rows={4}
                    text={groupData?.description}
                  />
                )}
              </div>

              <div className="pt-3 gap-2 cursor-pointer">
                {groupData.users?.map((user, i) => (
                  <div className="inline-flex p-1" key={i}>
                    <UserDelete userData={user} />
                  </div>
                ))}
              </div>
              <div className="flex w-full pt-3">
                <div className="flex h-12 overflow-x-auto scrollbar-hide hover:scrollbar-auto">
                  {tag?.map(
                    (tag, i) =>
                      tag.name !== "" &&
                      tag.count !== 0 && (
                        <div className="inline-flex p-1" key={i}>
                          <TagButton
                            color="rgb(85, 172, 238)"
                            tagName={tag.name}
                            key={i}
                          />
                        </div>
                      )
                  )}
                </div>
              </div>
              {deleteUserModal && (
                <DialogModal
                  open={deleteUserModal}
                  onClose={() => setDeleteUserModal(false)}
                  title={`Are you sure you want to delete this ${deleteUserName} user?`}
                >
                  {error.length !== 0 && (
                    <div className="flex items-center justify-center p-5 border-b border-solid border-slate-200 rounded-t">
                      <h5 className="text-xl font-medium text-red-600">
                        {error}
                      </h5>
                    </div>
                  )}
                  <DeleteModal
                    onClick={() => {
                      setLoading(true);
                      handleDeleteUser(delUser);
                    }}
                    onClose={() => setDeleteUserModal(false)}
                    disabled={loading}
                  />
                </DialogModal>
              )}
            </Card>
          </Space>
          <hr className="border border-t-[1px] border-solid border-slate-200 my-[2rem]" />
          {uploadModal && (
            <DialogModal
              open={uploadModal}
              onClose={() => setUploadModal(false)}
              title={`Upload a Media`}
            >
              {error.length !== 0 && (
                <div className="flex items-center justify-center p-5 border-b border-solid border-slate-200 rounded-t">
                  <h5 className="text-xl font-medium text-red-600">{error}</h5>
                </div>
              )}
              <UploadForm
                onClose={() => setUploadModal(false)}
                archiveName={groupData.name}
                notify={notifyToast}
              />
            </DialogModal>
          )}
          {importModal && (
            <DialogModal
              open={importModal}
              onClose={() => setImportModal(false)}
              title={"Import a Media"}
            >
              <ImportModal
                type={"media"}
                groupId={id}
                onClose={() => setImportModal(false)}
              />
            </DialogModal>
          )}
          {deleteCustomModal && (
            <DialogModal
              open={deleteCustomModal}
              onClose={() => {
                setDeleteCustomModal(false);
                setDeleteCustomQuestion(false);
              }}
              title={`${
                deleteCustomQuestion
                  ? `This action will delete the custom field along with default values. Are you sure you want to delete custom field ?`
                  : "Edit"
              } a Custom Question`}
            >
              <CustomModal
                groupId={id}
                onClose={() => {
                  setDeleteCustomModal(false);
                  getGroup();
                }}
                groups={grpCustom}
                deleteButton={deleteCustomQuestion}
              />
            </DialogModal>
          )}
          {editColModal && (
            <DialogModal
              open={editColModal}
              onClose={() => setEditColModal(false)}
              title={`Edit ${groupData.name}`}
            >
              <CreateCollection
                onClose={() => {
                  setEditColModal(false);
                  getGroup();
                }}
                path="edit"
              />
            </DialogModal>
          )}
          {addUserColModal && (
            <DialogModal
              open={addUserColModal}
              onClose={() => setAddUserColModal(false)}
              title={`Add users to ${groupData.name}`}
            >
              <AddUsersModal
                onClose={() => {
                  setAddUserColModal(false);
                  getGroup();
                }}
                onClick={() => {
                  getGroup();
                }}
                users={groupData.users}
                id={id}
              />
            </DialogModal>
          )}
          {customColModal && (
            <DialogModal
              open={customColModal}
              onClose={() => setCustomColModal(false)}
              title={`Add Custom Question to ${groupData.name}`}
            >
              <CreateCollection
                onClose={() => {
                  setCustomColModal(false);
                  getGroup();
                }}
                path="custom-question"
              />
            </DialogModal>
          )}
          {keepColModal && (
            <DialogModal
              open={keepColModal}
              onClose={() => setKeepColModal(false)}
              title={`How long to keep deleted data from ${groupData.name}`}
            >
              <CreateCollection
                onClose={() => {
                  setKeepColModal(false);
                  getGroup();
                }}
                path="keep-data"
              />
            </DialogModal>
          )}
        </div>
      </>
    );
  };

  const NoData = () => {
    const EmptyStates = () => {
      return (
        <div className="text-center justify-center font-semibold text-lg rounded-md">
          There are no media to view in this collection.
        </div>
      );
    };
    return <EmptyStates />;
  };

  const openModel = (value) => {
    const handleExport = (id: any, exporting: any) => {
      const options = {
        method: "POST",
        url: `${baseURL}api/v1/export/`,
        headers: {
          Authorization: `Token ${auth}`,
          "Content-Type": "application/json",
        },
        data: { type: exporting, id: id },
      };

      axios
        .request(options)
        .then((response) => {
          console.log(response.data);
          navigate("/requests");
        })
        .catch((error) => {
          console.error(error);
          setError(error.response.data);
        });
    };
    if (value?.id === "edit") {
      setEditModal(true);
      setModelOpen(true);
      setMediaId(value?.uuid);
      setMediaValue(value?.name);
      setMediaTags(value?.tags);
      setMediaDescription(value?.description);
    } else if (value?.id === "copy") {
      setCopyModal(true);
      setModelOpen(true);
      setMediaId(value?.uuid);
      setMediaValue(value?.name);
    } else if (value?.id === "delete") {
      setDeleteMediaModal(true);
      setModelOpen(true);
      setMediaId(value?.uuid);
      setMediaValue(value?.created_by);
    } else if (value?.id === "export") {
      handleExport(value?.uuid, "annotation");
    }
  };

  const Tabs = () => {
    return (
      <>
        <div className="flex flex-wrap">
          {count === 0 ? (
            <div className="w-full gap-5">
              <div className="mb-4">
                <CardLayout noDataText={<NoData />} />
              </div>
              {grpUsers.map(
                (user, i) =>
                  user.id === me?.id && (
                    <Col
                      xs={{ span: 24 }}
                      lg={{ span: 8 }}
                      className="cursor-pointer"
                      key={i}
                    >
                      <Card
                        style={{ borderColor: "rgb(179, 222, 236)" }}
                        onClick={() =>
                          auth ? setUploadModal(true) : navigate("/auth/login")
                        }
                        loading={loading}
                      >
                        <div className="grid place-items-center justify-center h-[13.2rem]">
                          <BsPlusSquareDotted
                            size={60}
                            style={{ textAlign: "center" }}
                          />
                          <span>Upload Media</span>
                        </div>
                      </Card>
                    </Col>
                  )
              )}
            </div>
          ) : (
            <div className="w-full">
              <Row gutter={[16, 16]}>
                {grpUsers.map(
                  (user, i) =>
                    user.id === me?.id && (
                      <Col
                        xs={{ span: 24 }}
                        lg={{ span: 8 }}
                        className="cursor-pointer"
                        key={i}
                      >
                        <Card
                          style={{ borderColor: "rgb(179, 222, 236)" }}
                          onClick={() =>
                            auth
                              ? setUploadModal(true)
                              : navigate("/auth/login")
                          }
                        >
                          <div className="grid place-items-center justify-center h-[16.2rem]">
                            <BsPlusSquareDotted
                              size={60}
                              style={{ textAlign: "center" }}
                            />
                            <span>Upload Media</span>
                          </div>
                        </Card>
                      </Col>
                    )
                )}
                {recordings?.map((res, i) => (
                  <Col xs={{ span: 24 }} lg={{ span: 8 }} key={i}>
                    <CardLayout
                      media={res}
                      loading={loading}
                      dropdown={
                        isMember === "true" && (
                          <AntDropDown
                            media={res}
                            dropIcon={"VerticalDots"}
                            modelOpen={openModel}
                            value={[
                              { id: "edit", displayName: "Edit" },
                              { id: "copy", displayName: "Copy" },
                              { id: "delete", displayName: "Delete" },
                              { id: "export", displayName: "Export" },
                            ]}
                          />
                        )
                      }
                      id={id}
                    />
                  </Col>
                ))}
              </Row>
              <div>
                <Pagination />
              </div>
            </div>
          )}
        </div>
      </>
    );
  };

  const Stats = () => {
    return (
      <>
        <div className="flex flex-wrap">
          <div className="w-full">
            <ul
              className="flex mb-0 list-none flex-wrap pt-3 pb-4 flex-row px-5"
              role="tablist"
            >
              <li className="-mb-px mr-2 last:mr-0 flex-auto text-center">
                <Button
                  size="sm"
                  type="tertiary"
                  className={
                    "w-full flex gap-5 uppercase px-5 py-3 shadow-lg rounded leading-normal " +
                    (openRadio === 1
                      ? "text-blue-500 bg-white"
                      : "text-white bg-blue-500")
                  }
                  onClick={(e) => {
                    e.preventDefault();
                    setOpenRadio(1);
                  }}
                  disabled={openRadio === 1 ? true : false}
                >
                  Sense Making Statistics
                  {openRadio === 1 && <MdCheckCircle size={15} />}
                </Button>
              </li>
              <li className="-mb-px mr-2 last:mr-0 flex-auto text-center">
                <Button
                  size="sm"
                  type="tertiary"
                  className={
                    "w-full flex gap-5 uppercase px-5 py-3 shadow-lg rounded leading-normal " +
                    (openRadio === 2
                      ? "text-blue-500 bg-white"
                      : "text-white bg-blue-500")
                  }
                  onClick={(e) => {
                    e.preventDefault();
                    setOpenRadio(2);
                  }}
                  disabled={openRadio === 2 ? true : false}
                >
                  Collection Statistics
                  {openRadio === 2 && <MdCheckCircle size={15} />}
                </Button>
              </li>
              <li className="-mb-px mr-2 last:mr-0 flex-auto text-center">
                <Button
                  size="sm"
                  type="tertiary"
                  className={
                    "w-full flex gap-5 uppercase px-5 py-3 shadow-lg rounded leading-normal " +
                    (openRadio === 3
                      ? "text-blue-500 bg-white"
                      : "text-white bg-blue-500")
                  }
                  onClick={(e) => {
                    e.preventDefault();
                    setOpenRadio(3);
                  }}
                  disabled={openRadio === 3 ? true : false}
                >
                  Tag Cloud Statistics
                  {openRadio === 3 && <MdCheckCircle size={15} />}
                </Button>
              </li>
            </ul>
            <div className="tab-content tab-space col-span-5">
              <div className={openRadio === 1 ? "block" : "hidden"} id="link1">
                <div className="items-center justify-center text-center place-items-center">
                  <div className="col-md-12">
                    <div className="font-bold text-3xl">
                      Sense Making Statistics
                    </div>
                  </div>
                </div>
                <div className="items-center justify-center text-center place-items-center">
                  {groupKnowledge.length !== 0 && (
                    <Graph
                      data={groupKnowledge}
                      layout={"circular"}
                      title={"Sense Making Statistics"}
                    />
                  )}
                </div>
              </div>
              <div className={openRadio === 2 ? "block" : "hidden"} id="link1">
                <div className="items-center justify-center text-center place-items-center">
                  <div className="col-md-12">
                    <div className="font-bold text-3xl">
                      Collection Statistics
                    </div>
                  </div>
                </div>
                <div className="grid lg:grid-cols-4 sm:col-span-1 md:col-span-1">
                  <div className="col-span-2 items-center justify-center text-center place-items-center">
                    <Line stats={mediaStatistics} label="Media" />
                  </div>
                  <div className="col-span-2 items-center justify-center text-center place-items-center">
                    <Line stats={annoStatistics} label="Annotation" />
                  </div>
                </div>
              </div>
              <div className={openRadio === 3 ? "block" : "hidden"} id="link2">
                <div className="items-center justify-center text-center place-items-center">
                  <div className="col-md-12">
                    <div className="font-bold text-3xl">
                      Tag Cloud Statistics
                    </div>
                  </div>
                </div>
                <div className="col-span-4 items-center justify-center text-center place-items-center">
                  {groupKnowledge.length !== 0 && (
                    <WordCloud
                      data={groupKnowledge}
                      layout={"none"}
                      title={"Tag Cloud Statistics"}
                    />
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  };

  if (error) {
    return <Loader error={error} />;
  }

  const updateModel = (value) => {
    if (value === "ok") {
      if (deleteMediaModal) {
        handleMedia(mediaId);
      }
      if (copyModal) {
      }
    }
    setModelOpen(false);
    setDeleteMediaModal(false);
    setCopyModal(false);
    setEditModal(false);
  };

  return (
    <div className="relative">
      <div className="container relative w-full p-2">
        {contextHolder}
        {modelOpen && (
          <>
            {deleteMediaModal ? (
              <AntModel
                modelTitle={`This action will delete the media and all annotations too. Are you sure you want to delete media created by ${mediaValue} ?`}
                modelOpen={modelOpen}
                updateModel={updateModel}
                okText={"Delete"}
                okType={"danger"}
              />
            ) : (
              copyModal && (
                <DialogModal
                  open={copyModal}
                  onClose={() => {
                    setCopyModal(false);
                    setModelOpen(false);
                  }}
                  title={"Copy Media to another collection"}
                >
                  <CopyModal
                    onClose={() => {
                      setCopyModal(false);
                      setModelOpen(false);
                    }}
                    mediaId={mediaId}
                    groupId={id}
                    groups={me.groups}
                    extra={copyExtra}
                  />
                </DialogModal>
              )
            )}
            {editModal && (
              <DialogModal
                open={editModal}
                onClose={() => {
                  setEditModal(false);
                  setModelOpen(false);
                }}
                title={`Edit media ${mediaValue}`}
              >
                {error.length !== 0 && (
                  <div className="flex items-center justify-center p-5 border-b border-solid border-slate-200 rounded-t">
                    <h5 className="text-xl font-medium text-red-600">
                      {error}
                    </h5>
                  </div>
                )}
                <EditUploadForm
                  onClose={() => {
                    setModelOpen(false);
                    setEditModal(false);
                  }}
                  notify={notifyToast}
                  mediaId={mediaId}
                  mediaTags={mediaTags}
                  mediaName={mediaValue}
                  mediaDescription={mediaDescription}
                  groupID={id}
                />
              </DialogModal>
              // <AntModel modelTitle={`Edit media ${mediaValue}`} modelOpen={modelOpen} updateModel={updateModel} />
            )}
          </>
        )}

        <div className="w-full">
          <GroupInfo />
          {relations ? <Stats /> : <Tabs />}
        </div>
      </div>
    </div>
  );
};

export default Collection;
