import About from "./About";
import Media from "./Media/Media";
import Dashboard from "./Dashboard/Dashboard";
import { Login } from "./Auth";
import Collection from "./Collection/Collection";

export { About, Media, Dashboard, Collection, Login }

