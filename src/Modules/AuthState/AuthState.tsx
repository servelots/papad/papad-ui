import { atom } from "recoil";

interface AuthDetails {
  first_name: string;
  groups: string[];
  id: string;
  is_superuser: boolean;
  last_name: string;
  username: string;
}

const loginState = atom({
  key: "loginState",
  default: {
    isLoading: false,
    isError: false,
    isSuccess: localStorage.getItem("token") ? true : false,
  },
});

const startTimer = atom({
  key: "startTimer",
  default: "00:00:00",
});

const endTimer = atom({
  key: "endTimer",
  default: "00:00:00",
});

const currentTimer = atom({
  key: "currentTimer",
  default: "0",
});

const durationTimer = atom({
  key: "durationTimer",
  default: "0",
});

const tokenState = atom({
  key: "tokenState",
  default: null,
});

const prodState = atom({
  key: "prodState",
  default: null,
});

const usernameState = atom({
  key: "usernameState",
  default: null,
});

const fullnameState = atom({
  key: "fullnameState",
  default: "",
});

const cardStartTimeState = atom({
  key: "cardStartTimeState",
  default: "",
});

const cardEndTimeState = atom({
  key: "cardEndTimeState",
  default: "",
});

const editAnnoIDState = atom({
  key: "editAnnoIDState",
  default: "",
});

const deleteAnnoIDState = atom({
  key: "deleteAnnoIDState",
  default: "",
});

const authData = atom<AuthDetails>({
  key: "authData",
  default: {
    first_name: "",
    last_name: "",
    username: "",
    id: "",
    groups: [],
    is_superuser: false,
  },
});

export {
  loginState,
  tokenState,
  prodState,
  usernameState,
  startTimer,
  endTimer,
  fullnameState,
  cardStartTimeState,
  editAnnoIDState,
  deleteAnnoIDState,
  currentTimer,
  durationTimer,
  authData,
  cardEndTimeState,
};
