# papad-ui

Papad UI. 
Papad UI to function it needs Papad-API, so its recommended to download [Papad-API](https://gitlab.com/servelots/papad/papad-api) and follow the intructions.
# Local Development

1. Copy the .env.sample as .env and edit to suit your needs. Edit REACT_APP_BASE_URL_LOCAL to your machine ipaddress:8000 Ex:REACT_APP_BASE_URL_LOCAL: "http://192.168.0.10:3000/"

2. Run a command to dev server in src folder:

3. In Papad-API edit AWS_S3_ENDPOINT:http://IP_ADDR:9000 Ex: AWS_S3_ENDPOINT:http://192.168.0.10:9000


```bash
yarn install
yarn dev
```

# Prod Development

Follow the first 3 steps from Local Development and start prod development in the root folder:

```bash
yarn install
serve -s build
```


