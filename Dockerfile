FROM node:18 as base

COPY . /app/
WORKDIR /app/
RUN yarn install
RUN echo "REACT_APP_COMMITID = $(git rev-parse --short HEAD)" >> /app/.env
RUN echo "REACT_APP_BRANCH = $(git symbolic-ref --short HEAD)" >> /app/.env
RUN echo "-==================-----------============="
RUN cat /app/.env
RUN echo "-==================-----------============="
RUN yarn build
from nginx 
COPY nginx/nginx.conf /etc/nginx/nginx.conf
COPY --from=base /app/build/ /app/site/
COPY startup.sh /app/startup.sh
CMD ["bash /app/startup.sh"]
